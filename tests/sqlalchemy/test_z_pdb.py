from tests.sqlalchemy.conftest import Fixture
from dotenv import load_dotenv

import pytest, os


@pytest.mark.test
def test_pdb_import(connection: Fixture):

    load_dotenv()
    itkdb_access_code1 = os.getenv("itkdb_access_code1")
    itkdb_access_code2 = os.getenv("itkdb_access_code2")

    db = connection.stage_db

    db.pdb_import(["20UPGM22110466", "20UPGM22110329"], itkdb_access_code1, itkdb_access_code2, "test")
    read_session = db.backend.create_read_session()

    tag = connection.stage_db.backend.read_tag_tree(read_session, "test", payload_data=True)
    read_session.close()

    root = tag["objects"][0]

    assert len(root["children"]) == 2
    assert root["children"][0]["metadata"]["serial"] == "20UPGM22110466" or root["children"][1]["metadata"]["serial"] == "20UPGM22110466"
    assert len(root["children"][0]["children"]) == 4
    assert len(root["children"][0]["children"][0]["payloads"][0]["data"]) > 10
    assert len(root["children"][0]["children"][0]["metadata"]["serial"]) > 5
