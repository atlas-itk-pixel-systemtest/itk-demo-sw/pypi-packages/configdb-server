from tests.sqlalchemy.conftest import Fixture
from configdb_server.testing_tools import extract_dict, flatten_list, extract_list, pop_ids, sort_list
from configdb_server.exceptions import WrongParameterError
from datetime import datetime

import pytest, json


def test_get_all(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    lst = db.get_all("payload", payload_data=True)

    output = extract_list(lst)
    input = flatten_list(extract_dict(data))

    sorted_input = sort_list(input)
    sorted_output = sort_list(output)

    assert sorted_input == sorted_output


def test_get_all_payload(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    session = db.backend.create_write_session()
    db.backend.create_payload(session, "meta", '{"test": "test"}', meta=True)
    session.close()

    lst = db.get_all("payload", payload_data=True)

    output = extract_list(lst)
    input = flatten_list(extract_dict(data))
    input.append({"type": "meta", "name": None, "data": {"test": "test"}})

    sorted_input = sort_list(input)
    sorted_output = sort_list(output)

    assert sorted_input == sorted_output


def test_get_all_time(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    lst = db.get_all("tag", payload_data=True)

    output = extract_list(lst)

    assert len(output) == 1
    try:
        datetime.strptime(output[0]["time"], "%Y-%m-%dT%H:%M:%S.%f")
    except ValueError:
        datetime.strptime(output[0]["time"], "%Y-%m-%dT%H:%M:%S")


def test_get_all_filter(connection_data: Fixture):
    db = connection_data.stage_db

    lst = db.get_all("payload", filter="config", payload_data=True)

    output = extract_list(lst)

    assert [{"type": "config", "name": "test", "data": "root_node payload"}] == output



def test_get_all_payload_filter(connection_data: Fixture):
    db = connection_data.stage_db

    lst = db.get_all("object", payload_filter="config", depth=0)

    output = []
    for item in lst:
        output.append(pop_ids(item))

    sort_list = sorted(output[0]["payloads"], key=lambda x: x["type"])

    assert [{"type": "config", "name": "test", "meta": False}, {"type": "connect", "name": "test", "meta": False}] == sort_list


def test_get_all_child_filter(connection_data: Fixture):
    db = connection_data.stage_db

    lst = db.get_all("object", child_filter="felix", depth=0)

    output = []
    for item in lst:
        output.append(pop_ids(item))

    sort_list = sorted(output[0]["payloads"], key=lambda x: x["type"])

    assert [{"type": "config", "name": "test", "meta": False}, {"type": "connect", "name": "test", "meta": False}] == sort_list


def test_get_all_order_by(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    lst = db.get_all("payload", payload_data=True, order_by="type")

    output = extract_list(lst)
    input = flatten_list(extract_dict(data))

    sorted_input = sort_list(input)
    sorted_output = sort_list(output)

    assert sorted_input == sorted_output


def test_get_all_order_by_desc(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    lst = db.get_all("payload", payload_data=True, order_by="type", asc=False)

    output = extract_list(lst)
    input = flatten_list(extract_dict(data))
    sorted_input = sorted(input, key=lambda x: x["type"])

    assert output[0]["type"] == "lpgbt"


def test_get_all_order_by_wrong(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    with pytest.raises(WrongParameterError):
        lst = db.get_all("payload", payload_data=True, order_by="wrong")


def test_get_all_tag_name(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    session = db.backend.create_write_session()
    db.backend.create_tag(session, "test_tag", "runkey")
    db.backend.create_tag(session, "tet_tag", "runkey")
    session.close()

    lst = db.get_all("tag", name_filter="test")

    assert len(lst) == 1



def test_runkey_with_payload(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payl_id = db.backend.create_payload(write_session, "test_payload", data=json.dumps({"test": "test"}), meta=True)
    db.backend.create_tag(write_session,"root_tag", "test", payloads=[payl_id])
    db.backend.create_tag(write_session,"root_tag2", "test", payloads=[payl_id])
    write_session.close()

    list = db.get_all("tag", connections=True)

    assert list[0]["metadata"]["test"] == "test"
    assert "data" not in list[0]["payloads"][0]
    assert list[1]["metadata"]["test"] == "test"
    assert "data" not in list[1]["payloads"][0]

def test_runkey_with_payload2(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payl_id = db.backend.create_payload(write_session, "test_payload", data=json.dumps({"test": "test"}), meta=True)
    db.backend.create_tag(write_session,"root_tag", "test", payloads=[payl_id])
    db.backend.create_tag(write_session,"root_tag2", "test", payloads=[payl_id])
    write_session.close()

    list = db.get_all("tag", payload_data=True, connections=True)

    assert list[0]["metadata"]["test"] == "test"
    assert "data" in list[0]["payloads"][0]
    assert list[1]["metadata"]["test"] == "test"
    assert "data" in list[1]["payloads"][0]

def test_tag_members(connection_member_data: Fixture):
    db = connection_member_data.stage_db

    list = db.get_all("tag", order_by="name", asc=False, connections=True)

    assert len(list[0]["groups"]) == 1
    assert len(list[1]["groups"]) == 1
    assert len(list[2]["groups"]) == 0
    assert len(list[0]["members"]) == 0
    assert len(list[1]["members"]) == 0
    assert len(list[2]["members"]) == 2