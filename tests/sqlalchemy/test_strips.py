from tests.sqlalchemy.conftest import Fixture
from configdb_server.exceptions import NotValidJSonError, DatasetNotFoundError
from configdb_server.database_tools import Backends

import pytest, json


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_reuse_id(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    with open("tests/sr1_strips.json", "r") as file:
        data = file.read()

    rk_name = "test"
    res1 = stage.create_full_tree(data=json.loads(data), name=rk_name)
    res2 = stage.commit(db, rk_name, rk_name, delete=False)
    tree = db.read_tree(rk_name)

    assert tree


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_delete(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    with open("tests/sr1_strips.json", "r") as file:
        data = file.read()

    rk_name = "test"
    res1 = stage.create_full_tree(data=json.loads(data), name=rk_name)
    session = stage.backend.create_write_session()
    res2 = stage.backend.delete_tree(session, rk_name)
    session.close()

    with pytest.raises(DatasetNotFoundError):
        tree = db.read_tree(rk_name)

    lis = ["closure", "payload", "object", "tag", "metadata"] # "tag_tag", "tag_object", "tag_metadata", "tag_payload", "object_payload", "object_metadata", "tag_object"]

    for i in lis:
        assert len(stage.get_all(i)) == 0
