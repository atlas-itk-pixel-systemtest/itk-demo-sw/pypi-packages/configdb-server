from tests.sqlalchemy.conftest import Fixture
from configdb_server.exceptions import DatasetNotFoundError
from configdb_server.adapter.base_adapter import Connection
from configdb_server.adapter.sqlalchemy.sqlalchemy_sqlite import SQLAlchemySQLiteAdapter
from configdb_server.database_tools import Database, Backends

import pytest, uuid


def test_delete_from_object(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child_id = db.backend.create_object(write_session, "test_child")
    payload_id = db.backend.create_payload(write_session, "test_payload", "test_data")
    metadata_id = db.backend.create_payload(write_session, "metadata", {"test": "test"}, meta=True)

    db.backend.add_to_object(write_session, object_id, [Connection(child_id)], [payload_id, metadata_id])
    new_object = db.read_tree(object_id, payload_data=True)

    db.backend.remove_from_object(
        write_session,
        object_id,
        [child_id],
        [metadata_id, payload_id],
    )
    write_session.close()

    payl = db.backend.read_payload(write_session, payload_id)
    meta = db.backend.read_payload(write_session, metadata_id)
    obj = db.backend.read_object(write_session, child_id)

    empty_object = db.read_tree(object_id)

    assert not empty_object["payloads"]
    assert not empty_object["children"]

    i = 0
    if new_object["payloads"][0]["type"] == "metadata":
        i = i + 1
    assert new_object["payloads"][i]["type"] == "test_payload"
    assert new_object["children"][0]["type"] == "test_child"

    assert len(new_object["payloads"]) == 2
    assert len(new_object["children"]) == 1

    assert obj["id"] == child_id
    assert payl["id"] == payload_id
    assert meta["id"] == metadata_id


def test_delete_from_object_view(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child_id = db.backend.create_object(write_session, "test_child")
    grandchild_id = db.backend.create_object(write_session, "test_grandchild")

    db.backend.add_to_object(write_session, object_id, [Connection(child_id)])
    db.backend.add_to_object(write_session, child_id, [Connection(grandchild_id)])

    db.backend.remove_from_object(
        write_session,
        child_id,
        [grandchild_id],
    )
    write_session.close()

    empty_object = db.read_tree(object_id)
    view_object = db.read_tree(object_id, view=3)

    assert not empty_object["children"][0]["children"]
    assert view_object["children"][0]["children"][0]["view"] == 2


def test_delete_not_exist(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")

    with pytest.raises(DatasetNotFoundError):
        db.backend.remove_from_object(
            write_session,
            object_id,
            [uuid.uuid4()],
        )
    write_session.close()


def test_delete_runkey(connection_data: Fixture):
    stage_db = connection_data.stage_db

    conn = stage_db.get_all("closure")
    payl = stage_db.get_all("payload")
    obj = stage_db.get_all("object")
    tag = stage_db.get_all("tag")

    write_session = stage_db.backend.create_write_session()
    stage_db.backend.delete_tree(write_session, "root_tag")
    write_session.close()

    empty_conn = stage_db.get_all("closure")
    empty_payl = stage_db.get_all("payload")
    empty_obj = stage_db.get_all("object")
    empty_tag = stage_db.get_all("tag")

    assert len(conn) > 0
    assert len(empty_conn) == 0
    assert len(payl) > 0
    assert len(empty_payl) == 0
    assert len(obj) > 0
    assert len(empty_obj) == 0
    assert len(tag) > 0
    assert len(empty_tag) == 0


def test_delete_runkey_stale(connection: Fixture):
    stage_db = connection.stage_db

    write_session = stage_db.backend.create_write_session()
    payload_id = stage_db.backend.create_payload(write_session, "felix", "payload_felix01", "test")
    root_uuid = stage_db.create_root(
        "root_tag",
        payloads=[
            {"data": "root_node payload", "type": "config", "name": "test"},
            {"data": "connectivity data", "type": "connect", "name": "test"},
        ],
    )
    felix1_uuid = stage_db.add_to_tree(
        "felix",
        parents=[Connection(root_uuid)],
        payloads=[
            {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
        ],
    )
    stage_db.backend.add_to_object(write_session, felix1_uuid, payloads=[payload_id])
    stage_db.add_to_tree("felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}])
    lpgbt1_uuid = stage_db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}])
    stage_db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}])
    stage_db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}])
    stage_db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe01", "type": "frontend", "name": "test"}])
    fe_uuid = stage_db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe02", "type": "frontend", "name": "test"}])

    stage_db.backend.remove_from_object(write_session, felix1_uuid, children=[lpgbt1_uuid])
    stage_db.backend.remove_from_object(write_session, felix1_uuid, payloads=[payload_id])
    write_session.close()

    conn = stage_db.get_all("closure")
    payl = stage_db.get_all("payload")
    obj = stage_db.get_all("object")
    tag = stage_db.get_all("tag")

    if isinstance(stage_db.backend, SQLAlchemySQLiteAdapter):
        stage_db = Database(Backends.SQLALCHEMY_SQLITE, "/tests/pytest_stage.db")
    write_session = stage_db.backend.create_write_session()
    stage_db.backend.delete_tree(write_session, "root_tag")
    write_session.close()

    empty_conn = stage_db.get_all("closure")
    empty_payl = stage_db.get_all("payload")
    empty_obj = stage_db.get_all("object")
    empty_tag = stage_db.get_all("tag")

    assert len(conn) > 0
    assert len(empty_conn) == 0
    assert len(payl) > 0
    assert len(empty_payl) == 1
    assert len(obj) > 0
    assert len(empty_obj) == 0
    assert len(tag) > 0
    assert len(empty_tag) == 0


def test_delete_runkey_keep_payl(connection_data: Fixture):
    stage_db = connection_data.stage_db

    conn = stage_db.get_all("closure")
    payl = stage_db.get_all("payload")
    obj = stage_db.get_all("object")
    tag = stage_db.get_all("tag")

    payload_id = payl[0]["id"]

    write_session = stage_db.backend.create_write_session()
    root_uuid = stage_db.create_root(
        "root_tag2",
        payloads=[payload_id],
    )
    write_session.close()

    write_session = stage_db.backend.create_write_session()
    stage_db.backend.delete_tree(write_session, "root_tag")
    write_session.close()

    empty_conn = stage_db.get_all("closure")
    empty_payl = stage_db.get_all("payload")
    empty_obj = stage_db.get_all("object")
    empty_tag = stage_db.get_all("tag")

    assert len(conn) > 0
    assert len(empty_conn) == 1
    assert len(payl) > 0
    assert len(empty_payl) == 1
    assert len(obj) > 0
    assert len(empty_obj) == 1
    assert len(tag) > 0
    assert len(empty_tag) == 1


def test_view_multiple_parents(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child1_id = db.backend.create_object(write_session, "test_child1")
    child2_id = db.backend.create_object(write_session, "test_child2")
    grandchild_id = db.backend.create_object(write_session, "test_grandchild")

    db.backend.add_to_object(write_session, object_id, [Connection(child1_id)])
    db.backend.add_to_object(write_session, object_id, [Connection(child2_id)])
    db.backend.add_to_object(write_session, child1_id, [Connection(grandchild_id)])
    db.backend.add_to_object(write_session, child2_id, [Connection(grandchild_id)])

    full_object = db.read_tree(object_id)

    db.backend.remove_from_object(
        write_session,
        child1_id,
        [grandchild_id],
    )
    write_session.close()

    view_object = db.read_tree(object_id, view=3)

    if view_object["children"][0]["children"][0]["view"] == 2:
        assert view_object["children"][1]["children"][0]["view"] == 1
    elif view_object["children"][1]["children"][0]["view"] == 2:
        assert view_object["children"][0]["children"][0]["view"] == 1
    else:
        assert False


def test_delete_read2(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child_id = db.backend.create_object(write_session, "test_child")
    grandchild_id = db.backend.create_object(write_session, "test_grandchild")

    db.backend.add_to_object(write_session, object_id, [Connection(child_id)])
    db.backend.add_to_object(write_session, child_id, [Connection(grandchild_id)])

    db.backend.remove_from_object(
        write_session,
        child_id,
        [grandchild_id],
    )
    write_session.close()

    view_object = db.read_tree(object_id, view=3)

    db.backend.add_to_object(write_session, child_id, [Connection(grandchild_id)])

    view_object2 = db.read_tree(object_id, view=3)

    assert view_object["children"][0]["children"][0]["view"] == 2
    assert view_object2["children"][0]["children"][0]["view"] == 1
