from tests.sqlalchemy.conftest import Fixture
from configdb_server.exceptions import PayloadFormatError, PayloadTypeError, NotValidJSonError, ChangeFormatError
from configdb_server.database_tools import Backends
from uuid import uuid4
import json, re, pytest


def test_json_bundle(connection: Fixture):
    db = connection.stage_db

    with open("tests/fe_config.json", "r") as f:
        fe_data = json.load(f)

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", fe_data)
    payl2 = db.backend.create_payload(session, "config", fe_data)
    payl3 = db.backend.create_payload(session, "config", fe_data)
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle["RD53A"]["Parameter"]["ChipId"] == [0, 0, 0]


def test_json_bundle_one(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    session.close()

    bundle = db.bundle_payloads([payl1])

    assert bundle == {"a": [1], "b": [[2], [3]], "c": {"d": [4]}, "e": [1]}


def test_json_bundle1(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [{"f": [{"g": 2}]}], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [{"f": [{"g": 4}]}], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [{"f": [{"g": 4}]}], "c": {"d": 3}, "e": 1}))
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle == {
        "a": [1, 2, 3],
        "b": [{"f": [{"g": [2, 4, 4]}]}],
        "c": {"d": [4, 4, 3]},
        "e": [1, 2, 1],
    }


def test_json_bundle2(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [{"h": 3}], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [{"h": 3}], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [{"h": 5}], "c": {"d": 3}, "e": 1}))
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle == {
        "a": [1, 2, 3],
        "b": [{"h": [3, 3, 5]}],
        "c": {"d": [4, 4, 3]},
        "e": [1, 2, 1],
    }


def test_json_bundle3(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"b": [[2, 1], 2]}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"b": [[3, 2], 3]}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"b": [[4, 5], 4]}))
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle == {
        "b": [[[2, 3, 4], [1, 2, 5]], [2, 3, 4]],
    }


def test_json_bundle4(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": [1, 2], "b": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": [3, 4], "b": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": [5, 6], "b": 3}))
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle == {"a": [[1, 3, 5], [2, 4, 6]], "b": [1, 2, 3]}


def test_json_bundle_full(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(
        session, "config", json.dumps({"a": 1, "b": [{"f": [{"g": 2}]}, {"h": 3}, [2, 1]], "c": {"d": 4}, "e": 1})
    )
    payl2 = db.backend.create_payload(
        session, "config", json.dumps({"a": 2, "b": [{"f": [{"g": 4}]}, {"h": 3}, [3, 2]], "c": {"d": 4}, "e": 2})
    )
    payl3 = db.backend.create_payload(
        session, "config", json.dumps({"a": 3, "b": [{"f": [{"g": 4}]}, {"h": 5}, [4, 5]], "c": {"d": 3}, "e": 1})
    )
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle == {
        "a": [1, 2, 3],
        "b": [{"f": [{"g": [2, 4, 4]}]}, {"h": [3, 3, 5]}, [[2, 3, 4], [1, 2, 5]]],
        "c": {"d": [4, 4, 3]},
        "e": [1, 2, 1],
    }


def test_json_bundle_meta(connection: Fixture):
    db = connection.stage_db

    with open("tests/fe_config.json", "r") as f:
        fe_data = json.load(f)

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", fe_data, meta=True)
    payl2 = db.backend.create_payload(session, "config", fe_data)
    payl3 = db.backend.create_payload(session, "config", fe_data, meta=True)
    session.close()

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    assert bundle["RD53A"]["Parameter"]["ChipId"] == [0, 0, 0]


def test_json_bundle_wrong_type(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", {})
    payl2 = db.backend.create_payload(session, "config2", {})
    session.close()

    with pytest.raises(PayloadTypeError):
        db.bundle_payloads([payl1, payl2])


def test_json_bundle_wrong_json(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", "string")
    payl2 = db.backend.create_payload(session, "config", {})
    session.close()

    with pytest.raises(NotValidJSonError):
        db.bundle_payloads([payl1, payl2])


# def test_json_bundle_wrong_format(connection: Fixture):
#     db = connection.stage_db

#     session = db.backend.create_write_session()
#     id1 = uuid4().hex
#     id2 = uuid4().hex
#     id3 = uuid4().hex
#     payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}), id=id1)
#     payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}}), id=id2)
#     payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}), id=id3)
#     session.close()

#     with pytest.raises(
#         PayloadFormatError, match=re.escape(f"Mismatch in payload format: Keys ['e'] from payload {id1} are missing in payload {id2}.")
#     ):
#         db.bundle_payloads([payl1, payl2, payl3])

def test_json_bundle_wrong_format2(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    id1 = uuid4().hex
    id2 = uuid4().hex
    id3 = uuid4().hex
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}}), id=id1)
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}}), id=id2)
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}), id=id3)
    session.close()

    with pytest.raises(
        PayloadFormatError, match=re.escape(f"Mismatch in payload format: Keys ['e'] from payload {id3} are missing in payload {id1}.")
    ):
        db.bundle_payloads([payl1, payl2, payl3])


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change(connection: Fixture):
    db = connection.stage_db

    with open("tests/fe_config.json", "r") as f:
        fe_data = json.load(f)

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", fe_data)
    payl2 = db.backend.create_payload(session, "config", fe_data)
    payl3 = db.backend.create_payload(session, "config", fe_data)
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])

    bundle = db.bundle_payloads([payl1, payl2, payl3])

    changes = {"RD53A": {"Parameter": {"ChipId": 1}}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    db.change_connected_payloads(connections, changes)

    obj = db.backend.read_object(session, obj)
    session.close()
    bundle2 = db.bundle_payloads([obj["payloads"][0], obj["payloads"][1], obj["payloads"][2]])

    assert bundle["RD53A"]["Parameter"]["ChipId"] == [0, 0, 0]
    assert bundle2["RD53A"]["Parameter"]["ChipId"] == [1, 1, 1]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_full(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"b": {0: 2, 2: 4}, "e": 5}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {"a": [1, 2, 3], "b": [[2, 2, 2], [3, 3, 5], [4, 4, 4]], "c": {"d": [4, 4, 3]}, "e": [5, 5, 5]}


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change1(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"list2": [{"a": 1}]}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"list2": [{"a": 2}]}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"list2": [{"a": 3}]}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"list2": {1: {"c": 3}}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {"list2": [{"a": [1, 2, 3]}, {"c": [3, 3, 3]}]}


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change2(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"list2": []}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"list2": []}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"list2": []}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"list2": {0: {"c": 3}}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {"list2": [{"c": [3, 3, 3]}]}


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change3(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [{"f": [{"g": [2]}]}]}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [{"f": [{"g": [4]}]}]}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [{"f": [{"g": [4]}]}]}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"b": {0: {"f": {0: {"g": {0: 1}}}}}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {"a": [1, 2, 3], "b": [{"f": [{"g": [[1, 1, 1]]}]}]}


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change4(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(
        session, "config", json.dumps({"a": 1, "b": [{"f": [{"g": [2]}]}, {"h": [3]}, [2, 1]], "c": {"d": 4}, "e": 1})
    )
    payl2 = db.backend.create_payload(
        session, "config", json.dumps({"a": 2, "b": [{"f": [{"g": [4]}]}, {"h": [3]}, [3, 2]], "c": {"d": 4}, "e": 2})
    )
    payl3 = db.backend.create_payload(
        session, "config", json.dumps({"a": 3, "b": [{"f": [{"g": [4]}]}, {"h": [5]}, [4, 5]], "c": {"d": 3}, "e": 1})
    )
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"b": {0: {"f": {0: {"g": {0: 1}}}}, 1: {"h": {1: 2}}, 2: {1: 1}}, "i": [2, 3, 4]}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {
        "a": [1, 2, 3],
        "b": [{"f": [{"g": [[1, 1, 1]]}]}, {"h": [[3, 3, 5], [2, 2, 2]]}, [[2, 3, 4], [1, 1, 1]]],
        "c": {"d": [4, 4, 3]},
        "e": [1, 2, 1],
        "i": [[2, 2, 2], [3, 3, 3], [4, 4, 4]],
    }


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_meta(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}), meta=True)
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}), meta=True)
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"b": {0: 2}, "e": 5}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {"a": [1, 2, 3], "b": [[2, 2, 2], [3, 3, 5]], "c": {"d": [4, 4, 3]}, "e": [5, 5, 5]}


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_not_exist(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"f": {"g": 1}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    bundle = db.bundle_payloads(ids)

    assert bundle == {"a": [1, 2, 3], "b": [[2, 4, 4], [3, 3, 5]], "c": {"d": [4, 4, 3]}, "e": [1, 2, 1], "f": {"g": [1, 1, 1]}}


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_no_change(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"b": {0: 2}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)

    assert len(ids) == 2


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_connected(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    obj1 = db.backend.create_object(session, "frontend", payloads=[payl1, payl2])
    obj2 = db.backend.create_object(session, "frontend", payloads=[payl1, payl2])

    changes = {"e": 0}
    connections = [
        {"payload_id": payl1, "object_id": obj1},
        {"payload_id": payl2, "object_id": obj1},
        {"payload_id": payl1, "object_id": obj2},
        {"payload_id": payl2, "object_id": obj2},
    ]

    obj1 = db.backend.read_object(session, obj1)
    obj2 = db.backend.read_object(session, obj2)
    session.close()
    ids = db.change_connected_payloads(connections, changes)

    assert len(ids) == 2
    assert obj1["payloads"] == obj2["payloads"]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_error(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"c": 5}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]

    with pytest.raises(
        ChangeFormatError, match=re.escape("Mismatch in change format: Change c = 5 expects int but payload contains dict")
    ):
        db.change_connected_payloads(connections, changes)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_error2(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"b": 5}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]

    with pytest.raises(
        ChangeFormatError, match=re.escape("Mismatch in change format: Change b = 5 expects int but payload contains list")
    ):
        db.change_connected_payloads(connections, changes)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_error3(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"a": [1]}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]

    with pytest.raises(
        ChangeFormatError, match=re.escape("Mismatch in change format: Change a = [1] expects dict/list but payload contains value")
    ):
        db.change_connected_payloads(connections, changes)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_change_error4(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1}))
    payl2 = db.backend.create_payload(session, "config", json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2}))
    payl3 = db.backend.create_payload(session, "config", json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1}))
    obj = db.backend.create_object(session, "frontend", payloads=[payl1, payl2, payl3])
    session.close()

    changes = {"a": {"1": 2}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]

    with pytest.raises(
        ChangeFormatError, match=re.escape("Mismatch in change format: Change a = {'1': 2} expects dict/list but payload contains value")
    ):
        db.change_connected_payloads(connections, changes)


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_json_format(connection: Fixture):
    db = connection.stage_db

    with open("tests/fe_config.json", "r") as f:
        fe_data = f.read()

    session = db.backend.create_write_session()
    payl1 = db.backend.create_payload(session, "config", fe_data)
    obj = db.backend.create_object(session, "frontend", payloads=[payl1])

    changes = {"RD53A": {"GlobalConfig": {"AdcRead": 1}}}
    connections = [
        {"payload_id": payl1, "object_id": obj},
    ]
    ids = db.change_connected_payloads(connections, changes)
    payl = db.backend.read_payload(session, ids[0], format=True)
    session.close()

    assert payl["data"].startswith('{\n    "RD53A": {\n        "GlobalConfig": {\n            "AdcRead": 1')

