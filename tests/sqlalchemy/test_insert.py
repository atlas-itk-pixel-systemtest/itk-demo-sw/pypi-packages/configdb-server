from configdb_server.tools import compressToBytes
from configdb_server.database_tools import Backends
from configdb_server.exceptions import FeatureNotSupported
from tests.sqlalchemy.conftest import Fixture
from uuid import uuid4

import pytest


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_insert_manual(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    payload_list = [
        {"type": "config", "data": compressToBytes("data1"), "id": uuid4().hex},
        {"type": "config", "data": compressToBytes("data2"), "id": uuid4().hex},
        {"type": "config", "data": compressToBytes("data3"), "id": uuid4().hex},
        {"type": "config", "data": compressToBytes("data4"), "id": uuid4().hex},
        {"type": "data", "data": compressToBytes("data5"), "id": uuid4().hex},
    ]
    db.backend.insert(write_session, "payload", payload_list)
    payloads = db.get_all("payload")

    object_list = [
        {"type": "frontend", "id": uuid4().hex},
        {"type": "frontend", "id": uuid4().hex},
        {"type": "frontend", "id": uuid4().hex},
        {"type": "felix", "id": uuid4().hex},
        {"type": "felix", "id": uuid4().hex},
    ]
    db.backend.insert(write_session, "object", object_list)

    association_list = [
        {"object_id": object_list[0]["id"], "payload_id": payload_list[0]["id"]},
        {"object_id": object_list[1]["id"], "payload_id": payload_list[1]["id"]},
        {"object_id": object_list[2]["id"], "payload_id": payload_list[2]["id"]},
        {"object_id": object_list[3]["id"], "payload_id": payload_list[3]["id"]},
        {"object_id": object_list[4]["id"], "payload_id": payload_list[4]["id"]},
    ]
    db.backend.insert(write_session, "object_payload", association_list)
    objects = db.get_all("object")
    write_session.close()

    assert len(payloads) == 5
    assert len(objects) == 5
    assert len(db.get_all("object", payload_filter="config")) == 4
    assert len(db.get_all("object", payload_filter="config", filter="felix")) == 1


@pytest.mark.parametrize("connection", [Backends.SQLALCHEMY_REC.name], indirect=True)
def test_insert_manual2(connection: Fixture):
    db = connection.db

    with pytest.raises(FeatureNotSupported):
        db.backend.insert("", "object_payload", [])


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_object_insert(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    child_id1 = db.backend.create_object(write_session, "test_child1")
    child_id2 = db.backend.create_object(write_session, "test_child2")
    parent_id1 = db.backend.create_object(write_session, "test_parent1")
    parent_id2 = db.backend.create_object(write_session, "test_parent2")
    write_session.close()

    list = [
        {
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
            "children": [child_id1, child_id2],
            "parents": [parent_id1, parent_id2],
        },
        {
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
            "children": [child_id1, child_id2],
            "parents": [parent_id1, parent_id2],
        },
        {
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
            "children": [child_id1, child_id2],
            "parents": [parent_id1, parent_id2],
        },
    ]

    object_ids = db.object_insert(list)

    payloads = db.get_all(
        "payload",
    )
    objects = db.get_all("object")

    root1 = db.read_tree(parent_id1, payload_data=True)
    root2 = db.read_tree(parent_id2, payload_data=True)

    assert len(object_ids) == 3
    assert len(payloads) == 6
    assert len(objects) == 7
    assert len(root1["children"]) == 3
    assert len(root1["children"][0]["payloads"]) == 2
    assert root1["children"][0]["payloads"][0]["data"] == "payload data"
    assert len(root1["children"][0]["children"]) == 2
    assert root1["children"] == root2["children"]


@pytest.mark.parametrize("connection", [Backends.SQLALCHEMY_REC.name], indirect=True)
def test_object_insert2(connection: Fixture):
    db = connection.db

    with pytest.raises(FeatureNotSupported):
        db.object_insert([])


@pytest.mark.parametrize("connection", [Backends.SQLALCHEMY_REC.name], indirect=True)
def test_insert_manual2(connection: Fixture):
    db = connection.db

    with pytest.raises(FeatureNotSupported):
        db.backend.insert("", "object_payload", [])


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_object_insert(connection: Fixture):
    db = connection.db

    object_id = uuid4().hex

    list = [
        {
            "id": object_id,
            "type": "frontend",
            "payloads": [
                {"type": "config", "data": "payload data", "name": "my_config"},
                {"type": "config", "data": "payload data", "name": "my_config"},
            ],
        }
    ]

    object_ids = db.object_insert(list)

    objects = db.get_all("object")

    assert len(object_ids) == 1
    assert len(objects) == 1
    assert objects[0]["id"] == object_id
