from tests.sqlalchemy.conftest import Fixture
from configdb_server.testing_tools import extract_dict, sort_list, std_data
from configdb_server.adapter.base_adapter import Connection

import copy


def test_remove_from_node(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, "felix", "payload_felix01", "test")

    root_uuid = db.create_root(
        "root_tag",
        payloads=[
            {"data": "root_node payload", "type": "config", "name": "test"},
            {"data": "connectivity data", "type": "connect", "name": "test"},
        ],
    )
    felix1_uuid = db.add_to_tree(
        "felix",
        parents=[Connection(root_uuid)],
        payloads=[
            {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
        ],
    )
    db.backend.add_to_object(write_session, felix1_uuid, payloads=[payload_id])
    db.add_to_tree("felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}])
    lpgbt1_uuid = db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe01", "type": "frontend", "name": "test"}])
    fe_uuid = db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe02", "type": "frontend", "name": "test"}])

    db.backend.remove_from_object(write_session, felix1_uuid, children=[lpgbt1_uuid])
    db.backend.remove_from_object(write_session, felix1_uuid, payloads=[payload_id])
    write_session.close()

    std_data_scan = copy.deepcopy(std_data)
    std_data_scan["children"][0]["children"].pop(0)
    std_data_scan["children"][0]["payloads"].pop(0)

    tree = db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(std_data_scan)

    assert sort_list(input) == sort_list(output)


def test_add_to_node(connection: Fixture):
    db = connection.db

    root_uuid = db.create_root(
        "root_tag",
        payloads=[
            {"data": "root_node payload", "type": "config", "name": "test"},
            {"data": "connectivity data", "type": "connect", "name": "test"},
        ],
    )
    felix1_uuid = db.add_to_tree(
        "felix",
        parents=[Connection(root_uuid)],
        payloads=[
            {"data": "payload_felix01", "type": "felix", "name": "test"},
            {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
        ],
    )
    db.add_to_tree("felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}])
    lpgbt1_uuid = db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe01", "type": "frontend", "name": "test"}])
    fe_uuid = db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe02", "type": "frontend", "name": "test"}])

    new_payload = {"data": "additional payload_fe02", "type": "frontend", "name": "test"}
    db.add_to_node(fe_uuid, payloads=[new_payload])
    std_data_scan = copy.deepcopy(std_data)
    std_data_scan["children"][0]["children"][0]["children"][1]["payloads"].append(new_payload)

    tree = db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(std_data_scan)

    assert sort_list(input) == sort_list(output)


def test_add_to_node_id(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    payload_id1 = db.backend.create_payload(write_session, "felix", "payload_felix01", "test")
    payload_id2 = db.backend.create_payload(write_session, "felix", "additional_payload_felix01", "test")
    write_session.close()

    root_uuid = db.create_root(
        "root_tag",
        payloads=[
            {"data": "root_node payload", "type": "config", "name": "test"},
            {"data": "connectivity data", "type": "connect", "name": "test"},
        ],
    )
    felix1_uuid = db.add_to_tree(
        "felix",
        parents=[Connection(root_uuid)],
        payloads=[payload_id1, payload_id2],
    )
    db.add_to_tree("felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}])
    lpgbt1_uuid = db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe01", "type": "frontend", "name": "test"}])
    fe_uuid = db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe02", "type": "frontend", "name": "test"}])

    new_payload = {"data": "additional payload_fe02", "type": "frontend", "name": "test"}
    db.add_to_node(fe_uuid, payloads=[new_payload])
    std_data_scan = copy.deepcopy(std_data)
    std_data_scan["children"][0]["children"][0]["children"][1]["payloads"].append(new_payload)

    tree = db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(std_data_scan)

    assert sort_list(input) == sort_list(output)
