from tests.sqlalchemy.conftest import Fixture
from configdb_server.exceptions import DatasetNotFoundError, FeatureNotSupported, IDInUseError
from configdb_server.testing_tools import reuse_data
from configdb_server.database_tools import Backends
from uuid import uuid4
import pytest


def test_reuse_payload(connection: Fixture):
    db = connection.stage_db
    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test", "id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    db.create_full_tree(data, "root_tag")
    tree = db.read_tree("root_tag", payload_data=True)

    payl = db.get_all("payload")

    assert len(payl) == 1

    assert tree["children"][0]["payloads"][0] == tree["children"][1]["payloads"][0]
    assert tree["children"][0]["payloads"][0] == tree["children"][2]["payloads"][0]
    assert tree["children"][1]["payloads"][0] == tree["children"][2]["payloads"][0]


def test_reuse_payload_twice(connection: Fixture):
    db = connection.stage_db
    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test", "id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    db.create_full_tree(data, "root_tag")

    with pytest.raises(IDInUseError):
        db.create_full_tree(data, "root_tag2")


def test_reuse_old_payload(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, "test_payload", "test_data")
    write_session.close()

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    db.create_full_tree(data, "root_tag")
    tree = db.read_tree("root_tag", payload_data=True)

    payl = db.get_all("payload")

    assert len(payl) == 1

    assert tree["children"][0]["payloads"][0] == tree["children"][1]["payloads"][0]
    assert tree["children"][0]["payloads"][0] == tree["children"][2]["payloads"][0]
    assert tree["children"][1]["payloads"][0] == tree["children"][2]["payloads"][0]


def test_reuse_wrong_payload(connection: Fixture):
    db = connection.stage_db

    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    with pytest.raises(DatasetNotFoundError):
        db.create_full_tree(data, "root_tag")


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_reuse_object(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, "test_payload", "test_data")
    write_session.close()

    object_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "felix",
                "children": [
                    {
                        "id": object_id,
                        "type": "optoboard",
                        "payloads": [{"reuse_id": payload_id}],
                    },
                ],
            },
            {"reuse_id": object_id},
        ],
    }

    db.create_full_tree(data, "root_tag")
    session = db.backend.create_read_session()
    tree = db.backend.read_tag_tree(session, "root_tag", payload_data=True)["objects"][0]
    session.close()

    if len(tree["children"][0]["children"]) == 1:
        assert tree["children"][0]["children"][0] == tree["children"][1]
    else:
        assert tree["children"][0] == tree["children"][1]["children"][0]


@pytest.mark.parametrize("connection", [Backends.SQLALCHEMY_REC.name], indirect=True)
def test_reuse_object2(connection: Fixture):
    db = connection.db

    object_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {"reuse_id": object_id},
        ],
    }

    with pytest.raises(FeatureNotSupported):
        db.create_full_tree(data, "root_tag")


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_reuse_payload_commit(connection: Fixture):
    stage_db = connection.stage_db
    db = connection.db
    payload_id = uuid4().hex

    data = {
        "type": "root",
        "children": [
            {
                "type": "optoboard",
                "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test", "id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
            {
                "type": "optoboard",
                "payloads": [{"reuse_id": payload_id}],
            },
        ],
    }

    stage_db.create_full_tree(data, "root_tag")
    tree1 = stage_db.read_tree("root_tag")
    stage_db.commit(db, "root_tag", "root_tag")
    tree2 = db.read_tree("root_tag")
    stage_db.clone(db, "root_tag", "root_tag2")
    tree3 = stage_db.read_tree("root_tag2")
    stage_db.commit(db, "root_tag2", "root_tag2")
    tree4 = db.read_tree("root_tag2")

    trees = [tree1, tree2, tree3, tree4]
    for tree in trees:
        assert tree["children"][0]["payloads"][0]["id"] == tree["children"][1]["payloads"][0]["id"]


@pytest.mark.parametrize(
    "connection",
    [Backends.SQLALCHEMY_SQLITE.name, Backends.SQLALCHEMY_MARIADB.name, Backends.SQLALCHEMY_POSTGRES.name, Backends.MARIADB.name],
    indirect=True,
)
def test_tree_example(connection: Fixture):
    db = connection.db

    root_id = db.create_full_tree(reuse_data, "root_tag")
    tree = db.read_tree("root_tag")

    found = False
    for child1 in tree["children"][0]["children"]:
        if child1["type"] == "A2":
            for child2 in tree["children"][1]["children"]:
                if child2["type"] == "A2":
                    assert child1 == child2
                    found = True
                    break
            if found:
                break

    assert found
