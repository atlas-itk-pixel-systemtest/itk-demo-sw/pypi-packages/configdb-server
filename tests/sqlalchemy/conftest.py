import pytest, subprocess
from configdb_server.database_tools import Database, Backends
from configdb_server.testing_tools import std_data, std_data_scan, std_data_small, std_data_meta
from time import sleep
import json


class Fixture:
    db: Database = ""
    stage_db: Database = ""
    data: list[dict] = []

    def __init__(self, db, stage_db) -> None:
        self.db = db
        self.stage_db = stage_db


@pytest.fixture(scope="session", autouse=True)
def setup_once():
    command = ["docker", "compose", "-f", "./tests/compose.yaml", "up", "-d"]
    process = subprocess.Popen(command, stdout=subprocess.PIPE)
    output, error = process.communicate()
    process.wait()


def create_connection(request) -> Fixture:
    backend = Backends[request.param]
    if backend == Backends.SQLALCHEMY_MARIADB or backend == Backends.MARIADB:
        stage_db = Database(backend, "configdb:test@localhost:3307/configdb")
        db = Database(backend, "configdb:test@localhost:3308/configdb")
    elif backend == Backends.SQLALCHEMY_POSTGRES:
        stage_db = Database(backend, "configdb:test@localhost:5430/configdb")
        db = Database(backend, "configdb:test@localhost:5431/configdb")
    else:
        # stage_db = Database(backend, "/file:stage.db?mode=memory&cache=shared&uri=true")
        # db = Database(backend, "/file:backend.db?mode=memory&cache=shared&uri=true")
        # ?mode=memory&cache=shared&uri=true

        stage_db = Database(backend, "/tests/pytest_stage.db")
        db = Database(backend, "/tests/pytest_backend.db")

    stage_db.init_database()
    db.init_database()

    db.backend.clear_database()
    stage_db.backend.clear_database()

    connection = Fixture(db, stage_db)
    return connection


@pytest.fixture(
    params=[
        Backends.SQLALCHEMY_REC.name,
        Backends.SQLALCHEMY_SQLITE.name,
        Backends.SQLALCHEMY_MARIADB.name,
        Backends.SQLALCHEMY_POSTGRES.name,
        Backends.MARIADB.name,
    ]
)  # Repeat for multiple backend types
def connection(request) -> Fixture:
    connection = create_connection(request)
    return connection

    # if backend == Backends.SQLALCHEMY_MARIADB:
    #     db.backend.clear_database()
    #     stage_db.backend.clear_database()


@pytest.fixture
def connection_data(connection: Fixture) -> Fixture:
    connection.data = std_data
    res = connection.stage_db.create_full_tree(connection.data, "root_tag")

    return connection


@pytest.fixture
def connection_member_data(connection: Fixture) -> Fixture:
    connection.data = std_data_small
    res = connection.stage_db.create_full_tree(connection.data, "root_tag")
    res2 = connection.stage_db.create_full_tree(connection.data, "root_tag2")

    session = connection.stage_db.backend.create_write_session()
    res3 = connection.stage_db.backend.create_tag(session, "bundle", "runkey", members=["root_tag", "root_tag2"])
    session.close()

    return connection


@pytest.fixture
def connection_meta_data(connection: Fixture) -> Fixture:
    connection.data = std_data_meta
    connection.stage_db.create_full_tree(connection.data, "root_tag")

    return connection


@pytest.fixture
def connection_scan_data(connection: Fixture) -> Fixture:
    connection.data = std_data_scan
    connection.stage_db.create_full_tree(connection.data, "root_tag")

    return connection


@pytest.fixture
def connection_small_data(connection: Fixture) -> Fixture:
    connection.data = std_data_small
    connection.stage_db.create_full_tree(connection.data, "root_tag")

    return connection


@pytest.fixture(
    params=[
        Backends.SQLALCHEMY_REC.name,
        Backends.SQLALCHEMY_SQLITE.name,
        Backends.SQLALCHEMY_MARIADB.name,
        Backends.SQLALCHEMY_POSTGRES.name,
        Backends.MARIADB.name,
    ]
)
def connection_search(request) -> Fixture:
    connection = create_connection(request)

    db = connection.stage_db

    write_session = db.backend.create_write_session()

    payl1 = db.backend.create_payload(write_session, type="as", data="as_data", name="as01")
    payl2 = db.backend.create_payload(write_session, type="ds", data="ds_data", name="ds01")
    payl3 = db.backend.create_payload(write_session, type="ts", data="ts_data", name="ts01")

    payl4 = db.backend.create_payload(write_session, type="as", data="as_data", name="as02")
    payl5 = db.backend.create_payload(write_session, type="ds", data="ds_data", name="ds02")
    payl6 = db.backend.create_payload(write_session, type="ts", data="ts_data", name="ts02")

    payl7 = db.backend.create_payload(write_session, type="as", data="as_data", name="as03")
    payl8 = db.backend.create_payload(write_session, type="ds", data="ds_data", name="ds03")
    payl9 = db.backend.create_payload(write_session, type="ts", data="ts_data", name="ts03")

    meta1 = db.backend.create_payload(write_session, type="meta", data=json.dumps({"serial": 1}), meta=True)
    meta2 = db.backend.create_payload(write_session, type="meta", data=json.dumps({"serial": 2}), meta=True)

    obj1 = db.backend.create_object(write_session, type="frontend", payloads=[payl1, payl2, payl3, meta1])
    obj2 = db.backend.create_object(write_session, type="frontend", payloads=[payl4, payl5, payl6, meta2])

    tag = db.backend.create_tag(write_session, name="tag1", type="scans", payloads=[payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9])

    write_session.close()

    return connection


# @pytest.fixture(scope="session", autouse=True)
def mariadb_setup():
    mariadb_start()
    yield
    mariadb_stop()


def mariadb_start():
    process = subprocess.Popen(["docker-compose", "-f", "tests/compose.yaml", "up", "-d", "--force-recreate"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    process.wait()
    sleep(5)
    # print(stdout.decode())
    # print(stderr.decode())


def mariadb_stop():
    process = subprocess.Popen(["docker-compose", "-f", "tests/compose.yaml", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    process.wait()
    # print(stdout.decode())
    # print(stderr.decode())
