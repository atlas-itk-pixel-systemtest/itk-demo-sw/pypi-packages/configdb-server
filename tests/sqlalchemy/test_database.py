from tests.sqlalchemy.conftest import Fixture
from configdb_server.database_tools import Backends, Database

def test_db_migration_status(connection: Fixture):
    stage = connection.stage_db
    db = connection.db

    db_session = db.backend.create_read_session()
    stage_session = stage.backend.create_read_session()

    assert stage.backend.database_migration_status(stage_session)
    assert db.backend.database_migration_status(db_session)

    db_session.close()
    stage_session.close()

def test_db_migration_status2():
    stage = Database(Backends.SQLALCHEMY_SQLITE, "/file:stage.db?mode=memory&cache=shared&uri=true")

    stage.backend.create_database()
    stage_session = stage.backend.create_read_session()

    assert not stage.backend.database_migration_status(stage_session)

    stage_session.close()
