import os
from pydantic import BaseModel

file_path = "/home/arbeit/itk-demo-sw/itk-demo-configdb/api/tests/test_configdb.py"
directory_name = os.path.dirname(file_path)
file_name = os.path.basename(file_path)

print(file_name)
print(directory_name)

class Test(BaseModel):
    test1: int = 1
    test2: int = 2

t = Test()

def tes(test):
    test.test1 = 2

tes(t)
print(t)