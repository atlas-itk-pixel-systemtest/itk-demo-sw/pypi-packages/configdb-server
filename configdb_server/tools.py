import json, gzip, os, re, uuid
from io import BytesIO
from uuid import uuid4


class ListGenerator:
    object_list = []
    payload_list = []
    object_payload_list = []
    metadata_list = []
    object_metadata_list = []
    closure_list = []
    fe_ids = []
    opto_ids = []
    felix_ids = []
    scan_ids = []
    result_ids = []
    parents = {}

    fe_config = None
    opto_config = None
    felix_config = None
    scan_config = None
    result_config = None
    root_id = None

    def __init__(
        self,
        fe_config,
        opto_config,
        felix_config,
        scan_config=None,
        result_config=None,
        root_id=None,
    ):
        self.fe_config = fe_config
        self.opto_config = opto_config
        self.felix_config = felix_config
        self.scan_config = scan_config
        self.result_config = result_config
        if root_id is None:
            self.root_id = uuid4().hex
            self.object_list.append({"type": "root", "id": self.root_id})
            self.closure_list.append({"descendant_id": self.root_id, "ancestor_id": self.root_id, "depth": 0})
        else:
            self.root_id = root_id

    def empty_lists(self):
        self.object_list = []
        self.payload_list = []
        self.object_payload_list = []
        self.metadata_list = []
        self.object_metadata_list = []
        self.closure_list = []

    def create_runkey_lists(self, fe_amount, opto_amount, felix_amount, scan_amount=0, result_amount=0):
        self.felix_ids = self._add_datasets(felix_amount, self.felix_config, "felix", [self.root_id], 1)
        self.opto_ids = self._add_datasets(opto_amount, self.opto_config, "optoboard", self.felix_ids, 2)
        self.fe_ids.extend(self._add_datasets(fe_amount, self.fe_config, "frontend", self.opto_ids, 3))

        if scan_amount > 0:
            meta = {
                "data": '{"scan_type": "digitalscan", "time": "2023-07-12 07:16:36", "author":"testing_tools"}',
                "meta": True,
                "type": "scan_data",
                "name": "meta",
            }
            self.scan_ids = self._add_datasets(scan_amount, self.scan_config, "scan", [self.root_id], 1, metadata=meta)
            self.result_ids = self._add_datasets(
                result_amount,
                self.result_config,
                "result",
                self.scan_ids,
                2,
                second_parent_ids=self.fe_ids,
            )

    def add_scan_to_runkey(self, scan_amount, result_amount, scan_config, result_config):
        if scan_amount > 0:
            meta = {
                "data": '{"scan_type": "digitalscan", "time": "2023-07-12 07:16:36", "author":"testing_tools"}',
                "meta": True,
                "type": "scan_data",
                "name": "meta",
            }
            self.scan_ids = self._add_datasets(scan_amount, scan_config, "scan", [self.root_id], 1, metadata=meta)
            self.result_ids = self._add_datasets(
                result_amount,
                result_config,
                "result",
                self.scan_ids,
                2,
                second_parent_ids=self.fe_ids,
            )

    def _add_datasets(
        self,
        amount,
        payload_data,
        type,
        parent_ids,
        depth,
        metadata=None,
        second_parent_ids=None,
    ):
        dataset_ids = []

        for i in range(0, amount):
            dataset_id = uuid4().hex
            self.object_list.append({"type": type, "id": dataset_id})

            payload_id = uuid4().hex
            self.payload_list.append({"type": f"{type}_config", "data": payload_data, "id": payload_id})
            self.object_payload_list.append({"object_id": dataset_id, "payload_id": payload_id})

            if metadata:
                metadata_id = uuid4().hex
                self.metadata_list.append({"type": f"{type}_config", "data": metadata, "id": metadata_id})
                self.object_metadata_list.append({"object_id": dataset_id, "metadata_id": metadata_id})

            self.parents[dataset_id] = parent_ids[i % len(parent_ids)]
            parent = dataset_id
            for j in range(1, depth + 1):
                parent = self.parents[parent]
                self.closure_list.append({"descendant_id": dataset_id, "ancestor_id": parent, "depth": j})

            if second_parent_ids:
                self.parents[dataset_id] = second_parent_ids[i]
                parent = dataset_id
                for j in range(1, depth + 1):
                    parent = self.parents[parent]
                    self.closure_list.append({"descendant_id": dataset_id, "ancestor_id": parent, "depth": j})

            self.closure_list.append({"descendant_id": dataset_id, "ancestor_id": dataset_id, "depth": 0})

            dataset_ids.append(dataset_id)
        return dataset_ids


def check_if_uuid(identifier: str):
    try:
        uuid.UUID(str(identifier))
        return True
    except ValueError:
        return False


def decompressBytes(inputBytes):
    """
    decompress the given byte array (which must be valid
    compressed gzip data) and return the decoded text (utf-8).
    """
    bio = BytesIO()
    stream = BytesIO(inputBytes)
    decompressor = gzip.GzipFile(fileobj=stream, mode="r")
    while True:  # until EOF
        chunk = decompressor.read(8192)
        if not chunk:
            decompressor.close()
            bio.seek(0)
            return bio.read().decode("utf-8")
        bio.write(chunk)
    return None


def compressToBytes(inputString):
    """
    read the given string, encode it in utf-8,
    compress the data and return it as a byte array.
    """
    bio = BytesIO()
    try:
        bio.write(inputString.encode("utf-8"))
    except AttributeError:
        bio.write(json.dumps(inputString).encode("utf-8"))
    bio.seek(0)
    stream = BytesIO()
    compressor = gzip.GzipFile(fileobj=stream, mode="w")
    while True:  # until EOF
        chunk = bio.read(8192)
        if not chunk:  # EOF?
            compressor.close()
            return stream.getvalue()
        compressor.write(chunk)


def flatten_dict(d, parent_key="", sep="."):
    items = []
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


class Runkey:
    object_list = []
    payload_list = []
    metadata_list = []
    object_payload_list = []
    object_metadata_list = []
    closure_list = []

    object_ids = []
    payload_ids = []
    metadata_ids = []
    tag_payload_ids = []
    tag_metadata_ids = []
    root_ids = []

    def __init__(
        self,
        object_list,
        payload_list,
        metadata_list,
        object_payload_list,
        object_metadata_list,
        closure_list,
        object_ids,
        payload_ids,
        metadata_ids,
        tag_payload_ids,
        tag_metadata_ids,
        root_ids,
    ):
        self.object_list = object_list
        self.payload_list = payload_list
        self.metadata_list = metadata_list
        self.object_payload_list = object_payload_list
        self.object_metadata_list = object_metadata_list
        self.closure_list = closure_list
        self.object_ids = object_ids
        self.payload_ids = payload_ids
        self.metadata_ids = metadata_ids
        self.tag_payload_ids = tag_payload_ids
        self.tag_metadata_ids = tag_metadata_ids
        self.root_ids = root_ids


def map_uuids(old_uuids):
    uuid_mapping = {}

    for old_uuid in old_uuids:
        if old_uuid not in uuid_mapping:
            uuid_mapping[old_uuid] = uuid.uuid4().hex

    return uuid_mapping
