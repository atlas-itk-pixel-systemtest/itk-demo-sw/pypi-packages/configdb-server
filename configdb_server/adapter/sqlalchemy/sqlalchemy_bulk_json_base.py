
from configdb_server.models.sqlalchemy.payload import Payload
from configdb_server.models.sqlalchemy.object import Object
from configdb_server.models.sqlalchemy.tag import Tag
from configdb_server.adapter.sqlalchemy.sqlalchemy_bulk_base import SQLAlchemyBulkAdapter
from configdb_server.tools import decompressBytes, check_if_uuid
from configdb_server.exceptions import DatasetNotFoundError
from sqlalchemy import select,  text
from collections import defaultdict

import json


class SQLAlchemyBulkJsonAdapter(SQLAlchemyBulkAdapter):
    def search_for_config(self, read_session, identifier: str, object_type: str = "", config_type: str = "", search_dict: dict = {}):
        from configdb_server.adapter.sqlalchemy.sqlalchemy_postgres import SQLAlchemyPostgresAdapter

        ids = self._search(read_session, identifier, object_type, search_dict)
        if ids == 0:
            return {}

        parameters = {}

        conf_type = ""
        conf_type_meta = ""

        if config_type != "":
            conf_type = " AND payload.type LIKE :config_type"
            conf_type_meta = " AND metadata.type LIKE :config_type"
            parameters["config_type"] = f"%{config_type}%"
            parameters["conf_type_meta"] = f"%{conf_type_meta}%"

        id_array = ""
        for i, id in enumerate(ids):
            id_array = id_array + f":id{i},"
            parameters[f"id{i}"] = id

        id_array = id_array[:-1]

        result = defaultdict(list)
        types = {}

        query = text(
            "SELECT object.id as object_id, object.type as object_type, payload.id, payload.type, payload.name, payload.data FROM object"
            + f" INNER JOIN object_payload ON object.id=object_payload.object_id AND object.id IN ({id_array})"
            + f" INNER JOIN payload ON object_payload.payload_id=payload.id{conf_type};"
        )
        payloads = read_session.execute(query, parameters).all()

        for payload in payloads:
            if isinstance(self, SQLAlchemyPostgresAdapter):
                payl_id = payload.id.hex
            else:
                payl_id = payload.id
            payl = {
                "id": payl_id,
                "type": payload.type,
                "name": payload.name,
                "data": decompressBytes(payload.data),
            }
            result[payload.object_id].append(payl)
            types[payload.object_id] = payload.object_type

        query = text(
            "SELECT object.id as object_id, object.type as object_type, metadata.id, metadata.type, metadata.name, metadata.data FROM object"
            + f" INNER JOIN object_metadata ON object.id=object_metadata.object_id AND object.id IN ({id_array})"
            + f" INNER JOIN metadata ON object_metadata.metadata_id=metadata.id{conf_type_meta}"
        )
        metas = read_session.execute(query, parameters).all()

        for meta in metas:
            if isinstance(self, SQLAlchemyPostgresAdapter):
                meta_id = meta.id.hex
                meta_data = json.dumps(meta.data)
            else:
                meta_id = meta.id
                meta_data = meta.data
            payl = {"id": meta_id, "type": meta.type, "name": meta.name, "data": meta_data}
            result[meta.object_id].append(payl)
            types[meta.object_id] = meta.object_type

        lis = []
        for key, value in result.items():
            lis.append({"id": key, "type": types[key], "payloads": value})

        return lis

    def search_for_subtree(
        self, read_session, identifier: str, object_type: str = "", search_dict: dict = {}, payload_data: bool = False, decode: bool = True, depth: int = -1, view=1
    ):
        ids = self._search(read_session, identifier, object_type, search_dict)
        if ids == 0:
            return {}

        lis = []
        for id in ids:
            lis.append(self.read_object_tree(read_session, id, payload_data=payload_data, decode=decode, depth=depth, view=view))

        return lis

    def _search(self, read_session, identifier: str, object_type: str = "", search_dict: dict = {}):
        from configdb_server.adapter.sqlalchemy.sqlalchemy_postgres import SQLAlchemyPostgresAdapter

        obj_type = ""
        parameters = {}
        if object_type != "":
            obj_type = text(" AND object.type LIKE :obj_type")
            parameters["obj_type"] = f"%{object_type}%"

        search = ""
        if search_dict:
            search = " INNER JOIN object_metadata ON object.id=object_metadata.object_id"
            search += " INNER JOIN metadata ON object_metadata.metadata_id=metadata.id"
            search, parameters = self._add_search_filter(search_dict, search, parameters)

        parameters["identifier"] = identifier
        if check_if_uuid(identifier) == True:
            runkey_query = text(f"SELECT object.id FROM object WHERE object.id=:identifier")
            query = text(
                f"SELECT object.id FROM closure" + f" INNER JOIN object ON closure.descendant_id=object.id AND closure.ancestor_id=:identifier{obj_type}" + f" {search};"
            )
        else:
            runkey_query = text(f"SELECT tag_object.object_id FROM tag INNER JOIN tag_object ON tag.id=tag_object.tag_id AND tag.name=:identifier")
            query = text(
                f"SELECT object.id FROM closure"
                + f" INNER JOIN object ON closure.descendant_id=object.id AND closure.ancestor_id IN ({runkey_query}){obj_type}"
                + f" {search};"
            )
        query = query.bindparams(**parameters)
        ids = read_session.execute(query, parameters).all()

        if len(ids) == 0:
            runkey = read_session.execute(runkey_query, parameters).all()
            if len(runkey) != 0:
                return 0
            else:
                raise DatasetNotFoundError

        response = []

        if isinstance(self, SQLAlchemyPostgresAdapter):
            for id in ids:
                response.append(id.id.hex)
        else:
            for id in ids:
                response.append(id.id)

        return response