from configdb_server.adapter.sqlalchemy.sqlalchemy_base import SQLAlchemyBaseAdapter
from configdb_server.adapter.base_adapter import Connection
from configdb_server.tools import check_if_uuid
from configdb_server.exceptions import DictTypeError, FeatureNotSupported, NotValidUUIDError


class SQLAlchemyRecAdapter(SQLAlchemyBaseAdapter):
    def read_object_tree(self, read_session, id, payload_data=False, payload_filter="", decode=True, format=False, depth=-1, view=1) -> dict:
        object = self.read_orm_object(read_session, id).to_tree(
            payload_data=payload_data, payload_filter=payload_filter, decode=decode, format=format, depth=depth, view=view
        )
        return object

    def read_tag_tree(self, read_session, name, payload_data=False, payload_filter="", decode=True, format=False, depth=-1, view=1) -> dict:
        tag = self.read_orm_tag(read_session, name).to_tree(
            payload_data=payload_data, payload_filter=payload_filter, decode=decode, format=format, depth=depth, view=view
        )
        return tag

    def write_full_tree(self, write_session, data: "list[dict]", encode: bool = True, keep_ids: bool = True) -> str:
        return self.__loop_write_tree(write_session, data, keep_ids, encode=encode).id

    def __loop_write_tree(self, write_session, data, keep_ids, encode=True):
        payload_objs = []
        children = []

        if "reuse_id" in data:
            raise FeatureNotSupported

        if "payloads" in data:
            for dataset in data["payloads"]:
                # reuse payload
                if "reuse_id" in dataset:
                    payload_id = dataset["reuse_id"]
                    if not check_if_uuid(payload_id):
                        NotValidUUIDError
                    payload_objs.append(payload_id)
                # create new payload
                else:
                    if "id" in dataset and keep_ids:
                        id = dataset["id"]
                        if not check_if_uuid(id):
                            raise NotValidUUIDError
                    else:
                        id = None
                    try:
                        payl = self.create_payload(
                            write_session,
                            dataset["type"],
                            dataset.get("data"),
                            name=dataset.get("name"),
                            id=id,
                            meta=dataset.get("meta"),
                            encode=encode,
                        )
                        payload_objs.append(payl)
                    except KeyError:
                        raise DictTypeError
                    # except IDInUseError:
                    #     payl = dataset["id"]
                    #     payload_objs.append(payl)

        if "children" in data:
            for dataset in data["children"]:
                children.append(self.__loop_write_tree(write_session=write_session, data=dataset, keep_ids=keep_ids, encode=encode))

        if "id" in data and keep_ids:
            id = data["id"]
            if not check_if_uuid(id):
                raise NotValidUUIDError
        else:
            id = None

        obj = self.create_object(write_session, data["type"], children=children, payloads=payload_objs, id=id)
        conn = Connection(id=obj, view=data.get("view", 1))

        return conn