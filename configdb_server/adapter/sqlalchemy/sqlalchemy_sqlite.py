from configdb_server.adapter.sqlalchemy.sqlalchemy_bulk_base import SQLAlchemyBulkAdapter
from sqlalchemy import create_engine, text


class SQLAlchemySQLiteAdapter(SQLAlchemyBulkAdapter):
    def __init__(self, owner_url, read_url="", write_url=""):
        self.owner_url = owner_url
        self.owner_engine = create_engine(self.owner_url, pool_pre_ping=True)

        if read_url != "":
            self.read_url = read_url
            self.read_engine = create_engine(self.read_url, connect_args={"check_same_thread": False}, pool_pre_ping=True)
        else:
            self.read_url = owner_url
            self.read_engine = self.owner_engine

        if write_url != "":
            self.write_url = write_url
            self.write_engine = create_engine(self.write_url, pool_pre_ping=True)
        else:
            self.write_url = owner_url
            self.write_engine = self.owner_engine

        session = self.create_owner_session()
        self.activate_foreign_key_constraints(session)
        session.close()

    def activate_foreign_key_constraints(self, session):
        session.execute(text("PRAGMA foreign_keys=ON"))
        session.commit()
        return True
