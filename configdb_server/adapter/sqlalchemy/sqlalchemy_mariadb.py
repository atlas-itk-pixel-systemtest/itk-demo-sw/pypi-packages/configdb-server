from configdb_server.adapter.sqlalchemy.sqlalchemy_bulk_json_base import SQLAlchemyBulkJsonAdapter
from configdb_server.models.sqlalchemy.metadata import Metadata
from configdb_server.tools import flatten_dict
from sqlalchemy import  func


class SQLAlchemyMariaDBAdapter(SQLAlchemyBulkJsonAdapter):
    def _add_search_filter(self, search_dict, search, parameters):
        search_dict = flatten_dict(search_dict)
        for i, (key, value) in enumerate(search_dict.items()):
            search += f" AND JSON_VALUE(metadata.data, :key{i}) = :value{i}"
            parameters[f"key{i}"] = f"$.{key}"
            parameters[f"value{i}"] = value
        return search, parameters

    def _get_filters(self, search_dict):
        filters = []
        for key, value in search_dict.items():
            filters.append(func.JSON_VALUE(Metadata.data, f"$.{key}") == value)
        return filters
