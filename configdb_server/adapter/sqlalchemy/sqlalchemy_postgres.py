from configdb_server.adapter.sqlalchemy.sqlalchemy_bulk_json_base import SQLAlchemyBulkJsonAdapter
from configdb_server.models.sqlalchemy.metadata import Metadata
from abc import ABC
from sqlalchemy import Integer, Float
import json


class SQLAlchemyPostgresAdapter(SQLAlchemyBulkJsonAdapter, ABC):
    def _add_search_filter(self, search_dict, search, parameters):
        search += " AND metadata.data @> :json"
        parameters["json"] = json.dumps(search_dict)
        return search, parameters

    def _get_filters(self, search_dict):
        filters = []
        for key, value in search_dict.items():
            if isinstance(value, int):
                filters.append(Metadata.data[key].astext.cast(Integer) == value)
            elif isinstance(value, float):
                filters.append(Metadata.data[key].astext.cast(Float) == value)
            else:
                filters.append(Metadata.data[key].astext == value)
        return filters
