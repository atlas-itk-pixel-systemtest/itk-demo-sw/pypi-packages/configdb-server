from configdb_server.adapter.base_adapter import BaseAdapter
from configdb_server.models.sqlalchemy.tag import Tag
from configdb_server.models.sqlalchemy.object import Object
from configdb_server.models.sqlalchemy.payload import Payload
from configdb_server.models.sqlalchemy.metadata import Metadata
from configdb_server.models.sqlalchemy.models import Base, object_payload, tag_object, tag_payload, object_metadata, tag_metadata, tag_tag, ObjectClosure
from configdb_server.profiling import SQLAlchemyProfiling
from configdb_server.tools import check_if_uuid
from sqlalchemy_utils import database_exists as database_exists_sqla, create_database as create_database_sqla
from sqlalchemy.exc import OperationalError, IntegrityError, ProgrammingError, CompileError
from configdb_server.exceptions import (
    NameInUseError,
    DatasetNotFoundError,
    WrongTableError,
    IDInUseError,
    WrongParameterError,
    EmptyTagError,
    FeatureNotSupported,
)
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker, Session
from abc import ABC
from alembic import command
from alembic.config import Config
from alembic.runtime.migration import MigrationContext
from alembic.script import ScriptDirectory
import contextlib, logging, datetime, os

# logger = logging.getLogger('sqlalchemy.engine')
# logger.propagate = False
# handler = logging.FileHandler('/itk-demo-sw/configdb_benchmarks/logs/sqlalchemy/file.log')
# formatter = logging.Formatter('%(asctime)s - %(message)s')
# handler.setFormatter(formatter)
# logger.addHandler(handler)
# logger.setLevel(logging.INFO)

main_tables = {
    "payload": Payload,
    "metadata": Metadata,
    "object": Object,
    "tag": Tag,
    "closure": ObjectClosure,
}

tables = {
    "object_payload": object_payload,
    "tag_object": tag_object,
    "tag_payload": tag_payload,
    "tag_metadata": tag_metadata,
    "object_metadata": object_metadata,
    "tag_tag": tag_tag,
}
tables.update(main_tables)


current_module_path = os.path.abspath(__file__)
dir = os.path.dirname(os.path.dirname(os.path.dirname(current_module_path)))

alembic_cfg = Config(os.path.join(dir, "alembic.ini"))
alembic_cfg.attributes["configure_logger"] = False
alembic_cfg.set_main_option("script_location", os.path.join(dir, "alembic"))


class SQLAlchemyBaseAdapter(BaseAdapter, ABC):
    def __init__(self, owner_url, read_url="", write_url=""):
        self.owner_url = owner_url
        self.owner_engine = create_engine(self.owner_url, pool_pre_ping=True)

        # @event.listens_for(self.owner_engine, "before_cursor_execute")
        # def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
        #     logger.info("Start Query:\n%s" % statement)

        if read_url != "":
            self.read_url = read_url
            self.read_engine = create_engine(self.read_url, pool_pre_ping=True)
        else:
            self.read_url = owner_url
            self.read_engine = self.owner_engine

        if write_url != "":
            self.write_url = write_url
            self.write_engine = create_engine(self.write_url, pool_pre_ping=True)
        else:
            self.write_url = owner_url
            self.write_engine = self.owner_engine

        self.profile = SQLAlchemyProfiling(self.owner_engine)

    def __del__(self):
        self.owner_engine.dispose()

    def create_owner_session(self):
        owner_session = sessionmaker(bind=self.owner_engine, future=True)()
        return owner_session

    def create_read_session(self):
        # self.read_engine.raw_connection().connection.text_factory = lambda x: x.decode(errors="ignore")
        read_session = sessionmaker(bind=self.read_engine, future=True)()
        return read_session

    def create_write_session(self):
        write_session = sessionmaker(bind=self.write_engine, future=True)()
        return write_session

    def read_tag(self, read_session, name) -> dict:
        tag = self.read_orm_tag(read_session, name).to_dict()
        return tag

    def read_orm_tag(self, read_session, name) -> Tag:
        dataset = read_session.query(Tag).filter_by(name=name).first()
        if dataset is None:
            raise DatasetNotFoundError
        return dataset

    def create_tag(
        self, write_session, name, type, objects=[], payloads=[], groups=[], members=[], author=None, id=None, comment=None, tag_latest=False
    ) -> str:
        time = datetime.datetime.now(datetime.timezone.utc)
        tag = Tag(
            session=write_session,
            name=name,
            type=type,
            objects=objects,
            payloads=payloads,
            groups=groups,
            members=members,
            author=author,
            id=id,
            comment=comment,
            time=time,
        )
        try:
            write_session.add(tag)
            if tag_latest:
                latest: Tag = write_session.query(Tag).filter_by(name="latest").first()
                if not latest:
                    self.create_latest_tag(write_session, objects=objects, payloads=payloads, groups=groups, members=members, time=time)
                else:
                    latest.update(write_session, objects=objects, payloads=payloads, delete=True, time=time)
            write_session.commit()
        except IntegrityError:
            write_session.rollback()
            raise NameInUseError(name)
        return tag.id.hex

    def create_latest_tag(self, write_session, objects, payloads, groups, members, time):
        tag = Tag(
            session=write_session,
            name="latest",
            type="runkey",
            objects=objects,
            payloads=payloads,
            groups=groups,
            members=members,
            comment="pointer on newest commit",
            time=time,
        )
        try:
            write_session.add(tag)
            # write_session.commit()
        except IntegrityError:
            write_session.rollback()
            raise NameInUseError("latest")

    # def update_tag(self, session, name, objects=[], payloads=[], metadata=[], delete=False) -> str:
    #     write_session = self.create_write_session()
    #     tag = write_session.query(Tag).filter_by(name=name).first()
    #     if tag is None:
    #         raise DatasetNotFoundError

    #     tag.update(write_session, objects, payloads, metadata, delete)
    #     write_session.commit()
    #     return tag.id.hex

    def add_to_tag(self, write_session, name, objects=[], payloads=[], groups=[], members=[]) -> str:
        tag = write_session.query(Tag).filter_by(name=name).first()
        if tag is None:
            write_session.rollback()
            raise DatasetNotFoundError

        tag.add_payloads(write_session, payloads)
        tag.add_objects(write_session, objects)
        tag.add_groups(write_session, groups)
        tag.add_members(write_session, members)
        write_session.commit()

    def remove_from_tag(self, write_session, name, objects=[], payloads=[], groups=[], members=[]) -> str:
        tag = write_session.query(Tag).filter_by(name=name).first()
        if tag is None:
            write_session.rollback()
            raise DatasetNotFoundError

        tag.remove_payloads(write_session, payloads)
        tag.remove_objects(write_session, objects)
        tag.remove_groups(write_session, groups)
        tag.remove_members(write_session, members)
        write_session.commit()

    def read_object(self, read_session, id, view: int = 1) -> dict:
        object = self.read_orm_object(read_session, id).to_dict(view=view)
        return object

    def read_orm_object(self, read_session, id) -> Object:
        return self.__query(read_session, Object, id)

    def create_object(self, write_session, type, children=[], parents=[], payloads=[], tags=[], id=None) -> str:
        with write_session.no_autoflush:
            object = Object(session=write_session, type=type, children=children, parents=parents, payloads=payloads, tags=tags, id=id)
        try:
            write_session.add(object)
            write_session.commit()
        except IntegrityError:
            write_session.rollback()
            raise IDInUseError
        return object.id.hex

    # def update_object(self, session, id, children=[], payloads=[], delete=False) -> str:
    #     object = self.__query(write_session, Object, id)
    #     if object is None:
    #         raise DatasetNotFoundError
    #     object.update(write_session, children, payloads, delete=False)
    #     write_session.commit()

    def add_to_object(self, write_session, id, children=[], payloads=[]) -> str:
        object: Object = self.__query(write_session, Object, id)
        object.add_payloads(write_session, payloads)
        object.add_children(write_session, children)
        write_session.commit()

    def remove_from_object(self, write_session, id, children=[], payloads=[]) -> str:
        object: Object = self.__query(write_session, Object, id)
        object.remove_payloads(write_session, payloads)
        object.remove_children(write_session, children)
        write_session.commit()

    def update_object_payload(self, write_session, id, payload_id, new_id):
        object: Object = self.__query(write_session, Object, id)
        object.remove_payloads(write_session, [payload_id])
        object.add_payloads(write_session, [new_id])
        write_session.commit()

    def update_connections(self, write_session, connections):
        for connection in connections:
            try:
                connection = connection.model_dump()
            except AttributeError:
                pass
            dataset = write_session.query(ObjectClosure).filter_by(ancestor_id=connection["parent"], descendant_id=connection["child"], depth=1).first()
            if dataset is None:
                DatasetNotFoundError
            else:
                dataset.view = connection["view"]
        write_session.commit()

    def read_payload(self, read_session, id, meta=False, format=False) -> dict:
        payload = self.read_orm_payload(read_session, id, meta).to_dict(format=format)
        return payload

    def read_orm_payload(self, read_session, id, meta) -> Payload:
        if meta:
            dataset = read_session.query(Metadata).filter_by(id=id).first()
        else:
            dataset = read_session.query(Payload).filter_by(id=id).first()
            if dataset is None:
                dataset = read_session.query(Metadata).filter_by(id=id).first()
        if dataset is None:
            raise DatasetNotFoundError
        return dataset

    def create_payload(self, write_session, type, data, name=None, id=None, meta=False, tags=[], objects=[], encode=True) -> str:
        if meta:
            dataset = Metadata(session=write_session, type=type, data=data, name=name, id=id, tags=tags, objects=objects)
        else:
            dataset = Payload(session=write_session, type=type, data=data, name=name, id=id, tags=tags, objects=objects, encode=encode)
        try:
            write_session.add(dataset)
            write_session.commit()
        except IntegrityError:
            write_session.rollback()
            raise IDInUseError
        return dataset.id.hex

    def update_payload(self, write_session, id, type="", data="", name="", encode=True, meta=False):
        dataset = self.read_orm_payload(write_session, id, meta)
        if data:
            dataset.set_data(data, encode)
        if type:
            dataset.type = type
        if name:
            dataset.name = name
        write_session.commit()
        return True

    def delete_tree(self, write_session, identifier) -> int:
        if check_if_uuid(identifier) == True:
            dataset = write_session.query(Object).filter_by(id=identifier).first()
        else:
            dataset = write_session.query(Tag).filter_by(name=identifier).first()

        if dataset is None:
            write_session.rollback()
            raise DatasetNotFoundError

        id = dataset.id.hex
        write_session.delete(dataset)
        write_session.commit()

        return id

    def get_ancestors(self, read_session, id: str, depth_offset: int = 0, view: int = 1) -> list[dict]:
        data = read_session.query(ObjectClosure).filter_by(descendant_id=id)
        if data is None:
            raise DatasetNotFoundError

        ancestors = []
        for ancestor in data:
            ancestors.append({"id": ancestor.ancestor_id.hex, "depth": ancestor.depth + depth_offset, "view": view})
        return ancestors

    def get_descendants(self, read_session, id: str, depth_offset: int = 0, view: int = 1) -> list[dict]:
        data = read_session.query(ObjectClosure).filter_by(ancestor_id=id)
        if data is None:
            raise DatasetNotFoundError

        descendants = []
        for descendant in data:
            descendants.append({"id": descendant.descendant_id.hex, "depth": descendant.depth + depth_offset, "view": view})
        return descendants

    def read_table(
        self,
        read_session,
        table,
        filter="",
        name_filter="",
        payload_filter="",
        child_filter="",
        offset=0,
        limit=0,
        order_by="",
        asc=True,
        payload_data=False,
        depth=0,
        connections=False,
    ) -> list[dict]:
        try:
            tbl_cls = main_tables[table.lower()]
        except KeyError:
            raise WrongTableError

        data = read_session.query(tbl_cls)

        if filter != "":
            data = data.filter_by(type=filter)
        if name_filter != "":
            if tbl_cls == Tag or tbl_cls == Payload:
                data = data.filter(tbl_cls.name.like(f"%{name_filter}%"))
        if order_by != "":
            if not asc:
                data = data.order_by(desc(order_by))
            else:
                data = data.order_by(order_by)
        if payload_filter == "" and child_filter == "":
            if offset != 0:
                data = data.offset(offset)
            if limit != 0:
                data = data.limit(limit)

        list = []
        dict_list = []

        if payload_filter or child_filter:
            connections = True

        try:
            data.first()
        except CompileError:
            raise WrongParameterError

        for dataset in data:
            found = True
            if connections:
                if payload_filter != "" and table != "payload":
                    found = False
                    for payload in dataset.payloads:
                        if payload.type == payload_filter:
                            found = True
                            break

                if child_filter != "":
                    found = False
                    for association in dataset.descendant_associations:
                        if association.depth == 1:
                            child = association.descendant
                            if child.type == child_filter:
                                found = True
                                break

            if found:
                list.append(dataset)
                if connections:
                    dict_list.append(dataset.to_tree(payload_data=payload_data, depth=depth))
                else:
                    dict_list.append(dataset.to_dict(connections=connections, payload_data=payload_data))

        return dict_list

    def __query(self, session: Session, cls, id: int):
        dataset = session.query(cls).filter_by(id=id).first()
        if dataset is None:
            raise DatasetNotFoundError
        return dataset

    def search_for_config(self, read_session, identifier: str, object_type: str = "", config_type: str = "", search_dict: dict = {}):
        result, types = self._search(read_session, identifier, object_type, search_dict, False)

        lis = []
        for obj_id, payloads in result.items():
            dic = {"id": obj_id, "type": types[obj_id]}
            payload_files = []
            for payload in payloads:
                # if not payload["type"].startswith("meta"):
                if config_type == "" or config_type in payload["type"]:
                    id = payload["id"]
                    payload_files.append(self.read_payload(read_session, id))
            dic["payloads"] = payload_files
            lis.append(dic)
        return lis

    def search_for_subtree(
        self,
        read_session,
        identifier: str,
        object_type: str = "",
        search_dict: dict = {},
        payload_data: bool = False,
        decode: bool = True,
        depth: int = -1,
        view: int = 1,
    ):
        result, types = self._search(read_session, identifier, object_type, search_dict, subtree=True)
        lis = []
        for res in result:
            lis.append(self.read_object_tree(read_session=read_session, id=res, payload_data=payload_data, decode=decode, depth=depth, view=view))
        return lis

    def _search(self, read_session, identifier: str, object_type: str = "", search_dict: dict = {}, subtree: bool = False):
        runkey = self.read_tree(read_session, identifier, payload_data=False, decode=False, depth=-1)

        result = {}
        types = {}
        depth_first_search(runkey, object_type, search_dict, result, types, subtree)
        return result, types

    def read_tree(
        self,
        read_session,
        identifier: str,
        payload_data: bool = False,
        payload_filter: str = "",
        decode: bool = True,
        format: bool = False,
        depth: int = -1,
        view: int = 1,
    ):
        if check_if_uuid(identifier):
            tree = self.read_object_tree(
                read_session=read_session,
                id=identifier,
                payload_data=payload_data,
                payload_filter=payload_filter,
                decode=decode,
                format=format,
                depth=depth,
                view=view,
            )
        else:
            tree = self.read_tag_tree(
                read_session=read_session,
                name=identifier,
                payload_data=payload_data,
                payload_filter=payload_filter,
                decode=decode,
                format=format,
                depth=depth,
                view=view,
            )
            length = len(tree["objects"])
            if length == 0:
                raise EmptyTagError
            elif length != 1:
                logging.warning("Tag has more than one object, using first one.")
            tree = tree["objects"][0]
        return tree

    # Database functions
    def __database_exists(self):
        try:
            return database_exists_sqla(self.owner_url)
        except TypeError:
            pass

    def database_running(self) -> bool:
        try:
            # Create an engine
            engine = create_engine(self.owner_url)

            # Try to establish a connection
            with engine.connect() as connection:
                pass
        except OperationalError:
            # If an OperationalError is raised, the connection could not be established
            return False
        return True

    def database_exists(self) -> bool:
        if database_exists_sqla(self.owner_url):
            return True
        return False

    def create_database(self):
        try:
            if not self.database_exists():
                create_database_sqla(self.owner_url)
        except (AttributeError, TypeError, OperationalError):
            pass

        # self.__init__(self.owner_url, self.read_url, self.write_url)

    def database_migration_status(self, read_session) -> bool:
        try:
            # Get the current database schema version
            conn = read_session.connection()
            context = MigrationContext.configure(conn)
            current_rev = context.get_current_revision()

            # Get the latest revision from the Alembic migration scripts
            dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
            alembic_cfg_path = os.path.join(dir, "alembic.ini")
            alembic_cfg = Config(alembic_cfg_path)
            script = ScriptDirectory.from_config(alembic_cfg)
            latest_rev = script.get_current_head()

            return current_rev == latest_rev
        except OperationalError:
            return False

    def upgrade_database(self):
        alembic_cfg.set_main_option("sqlalchemy.url", self.owner_url)
        command.upgrade(alembic_cfg, "head")

        # Base.metadata.create_all(self.owner_engine)
        # self.__init__(self.owner_url, self.read_url, self.write_url)

    def downgrade_database(self):
        alembic_cfg.set_main_option("sqlalchemy.url", self.owner_url)
        command.downgrade(alembic_cfg, "-1")

    def clear_database(self):
        with contextlib.closing(self.owner_engine.connect()) as con:
            trans = con.begin()
            for table in reversed(Base.metadata.sorted_tables):
                try:
                    con.execute(table.delete())
                except ProgrammingError:
                    pass
            trans.commit()

    def insert(self, write_session, table, list, encode=False):
        raise FeatureNotSupported

    def read_lists(self, read_session, identifier: str, payload_data: bool = False, decode: bool = True, depth: int = -1, view: int = 1):
        raise FeatureNotSupported

    def write_lists(self, write_session, lists: "list[list]", encode: bool = True, keep_ids: bool = True):
        raise FeatureNotSupported

    def search_in_tag(self, read_session, name: str, payload_types: list[str], search_dict: dict) -> list:
        raise FeatureNotSupported

    def _get_filters(self, search_dict):
        raise FeatureNotSupported


def depth_first_search(dataset, type, search_dict, result, types, subtree=False):
    # add = -1: nothing, 0: payloads, 1: subtree
    if type == "" or dataset["type"] in type:
        all_found = True
        for key, item in search_dict.items():
            if not key in dataset["metadata"] or dataset["metadata"][key] != item:
                all_found = False
        if all_found:
            types[dataset["id"]] = dataset["type"]
            if not subtree:
                result[dataset["id"]] = dataset["payloads"]
            else:
                result[dataset["id"]] = dataset

    if "children" not in dataset:
        return None

    for child in dataset["children"]:
        depth_first_search(child, type, search_dict, result, types, subtree)

    return None
