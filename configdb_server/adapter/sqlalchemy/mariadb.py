from mysql.connector import connect
from mysql.connector.errors import IntegrityError, OperationalError
from urllib.parse import urlparse

from sqlalchemy import Table
from configdb_server.adapter.sqlalchemy.sqlalchemy_mariadb import SQLAlchemyMariaDBAdapter
from configdb_server.adapter.sqlalchemy.sqlalchemy_base import tables
from configdb_server.tools import compressToBytes
from configdb_server.profiling import ConnectorProfiling
from configdb_server.exceptions import WrongTableError, IDInUseError, NotValidJSonError, PacketSizeError, DatasetNotFoundError
import json, re


class MariaDBAdapter(SQLAlchemyMariaDBAdapter):
    def __init__(self, owner_url, read_url="", write_url=""):
        super().__init__(owner_url, read_url, write_url)
        parsed_url = urlparse(owner_url)
        self.cnx = connect(
            user=parsed_url.username,
            password=parsed_url.password,
            host=parsed_url.hostname,
            port=parsed_url.port,
            database=parsed_url.path.lstrip("/"),
        )

        self.profile = ConnectorProfiling(self.cnx)

    def __del__(self):
        super().__del__()
        self.cnx.close()

    def execute(self, session, query):
        cursor = self.cnx.cursor()
        cursor.execute(query)
        ret = cursor.fetchall()
        cursor.close()
        return ret

    def insert(self, write_session, table, list, encode=False):
        table = table.lower()
        try:
            tbl_cls = tables[table]
        except KeyError:
            raise WrongTableError

        query = self.construct_insert_query(
            tbl_cls,
        )

        if list:
            if table == "payload" or table == "metadata":
                for data_dict in list:
                    data_dict.setdefault("name", "")
                    data_dict.setdefault("data", "")

            if table == "payload" and encode:
                for dataset in list:
                    if dataset["data"]:
                        dataset["data"] = compressToBytes(dataset["data"])

            if table == "metadata":
                for dataset in list:
                    if isinstance(dataset["data"], str):
                        try:
                            json.loads(dataset["data"])
                        except json.JSONDecodeError:
                            raise NotValidJSonError
                    else:
                        dataset["data"] = json.dumps(dataset["data"])

            cursor = self.cnx.cursor()
            try:
                cursor.executemany(query, list)
            except OperationalError:
                self.cnx.rollback()
                cursor.close()
                raise PacketSizeError
            except IntegrityError as e:
                write_session.rollback()
                if str(e).endswith("for key 'PRIMARY'"):
                    raise IDInUseError(reg=str(e))
                else:
                    raise DatasetNotFoundError
            try:
                self.cnx.commit()
            except IntegrityError:
                self.cnx.rollback()
                cursor.close()
                raise IDInUseError

            cursor.close()

    def construct_insert_query(self, tbl):
        if isinstance(tbl, Table):
            tablename = tbl.name
            columns = tbl.columns
        else:
            tablename = tbl.__tablename__
            columns = tbl.__table__.columns

        query = f"INSERT INTO {tablename} ("
        for column in columns:
            query += column.name + ", "
        query = query[:-2] + ") VALUES ("
        for column in columns:
            query += f"%({column.name})s, "
        query = query[:-2] + ")"
        return query
