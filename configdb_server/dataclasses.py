from pydantic import BaseModel, Field, AfterValidator
from typing_extensions import Annotated
from typing import Generic, TypeVar, Union, ForwardRef, Any, List, Optional
from uuid import uuid4

TypeX = TypeVar("TypeX")  # for responses

ReuseObjectModel = ForwardRef("ReuseObjectModel")
ObjectTreeModel = ForwardRef("ObjectTreeModel")
ReuseConnectionModel = ForwardRef("ReuseConnectionModel")
ReuseModel = ForwardRef("ReuseModel")
PayloadModel = ForwardRef("PayloadModel")
ConnectionModel = ForwardRef("ConnectionModel")


def convert_id(v):
    if not isinstance(v, ConnectionModel):
        con = ConnectionModel(id=v)
        return con
    else:
        return v


def convert_ids(v):
    for i, value in enumerate(v):
        if not isinstance(value, ConnectionModel):
            v[i] = ConnectionModel(id=value)
    return v


IDFieldDef = Field(
    min_length=32,
    max_length=36,
    pattern="^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{12}$",
    description="uuid of the dataset as a string without dashes",
    examples=[uuid4().hex],
)
NameFieldDef = Field(
    description="name of the tag",
    examples=["my_runkey_name"],
)

OptionalIDField = Annotated[Optional[str], IDFieldDef]
IDField = Annotated[str, IDFieldDef]

ConnectionField = Annotated[
    Union[ConnectionModel, IDField],
    AfterValidator(convert_id),
    Field(description="connection to another object"),
]
ViewField = Annotated[
    int,
    Field(description="view of the tree (e.g.: 1 for default, 2 for stale, 3 for both)"),
]
PayloadListField = Annotated[List[IDField], Field(description="list of payload UUIDs")]
ObjectListField = Annotated[List[IDField], Field(description="list of object UUIDs")]
PayloadReuseField = Annotated[
    List[Union[ReuseModel, PayloadModel]],
    Field(description="list of payload datasets with option to reuse existing payloads"),
]
ObjectReuseField = Annotated[
    List[Union[ReuseConnectionModel, ReuseObjectModel]],
    Field(description="list of object datasets with option to reuse existing object"),
]
ChildrenConnField = Annotated[List[ConnectionField], Field(description="list of children connections")]
PayloadIDListField = Annotated[
    List[Union[PayloadModel, IDField]],
    Field(description="list of payload datasets or uuids"),
]
TagTypeField = Annotated[str, Field(description="type of the tag", examples=["runkey"])]
PDBCodeField = Annotated[str, Field(description="access code for the pdb")]
TagNameField = Annotated[str, NameFieldDef]
TagNameListField = Annotated[List[TagNameField], Field(description="list of tag names")]
PayloadTypeField = Annotated[str, Field(description="type of the payload", examples=["configuration"])]

SearchField = Annotated[dict[str, Any], Field(description="dict with key-value pairs to search for")]


class ConnectionModel(BaseModel):
    id: IDField
    view: ViewField = 1


class PayloadModel(BaseModel):
    id: OptionalIDField = None
    name: Annotated[str, Field(description="name of the payload", examples=["fe_config.json"])] = None
    type: PayloadTypeField
    data: Annotated[Union[dict[str, Any], str], Field(description="data of the payload", examples=["payload_data as string"])] = ""
    meta: Annotated[bool, Field(description="save payload as searchable metadata")] = False


class BasicPayloadModel(PayloadModel):
    objects: ObjectListField = []
    tags: TagNameListField = []


class PayloadIDModel(PayloadModel):
    id: IDField


class ObjectBaseModel(BaseModel):
    id: OptionalIDField = None
    type: Annotated[str, Field(description="type of the object", examples=["frontend"])]
    children: ChildrenConnField = []
    payloads: PayloadListField = []


class ObjectModel(ObjectBaseModel):
    parents: Annotated[List[ConnectionField], Field(description="list of parent connections")] = []


class BasicObjectModel(ObjectModel):
    tags: TagNameListField = []


class ObjectPayloadModel(ObjectModel):
    payloads: PayloadIDListField = []


class BasicObjectPayloadModel(ObjectPayloadModel):
    tags: TagNameListField = []


class ObjectTreeModel(ObjectPayloadModel):
    children: Annotated[List[ObjectTreeModel], Field(description="list of object datasets")] = []


class TagBaseModel(BaseModel):
    id: OptionalIDField = None
    name: TagNameField
    type: TagTypeField
    author: Annotated[
        str,
        Field(description="name of the author of the tag", examples=["Max Mustermann"]),
    ] = None
    comment: Annotated[
        str,
        Field(description="comment for the tag", examples=["comment text"]),
    ] = None


class RootTagModel(TagBaseModel):
    payloads: Annotated[
        List[PayloadModel],
        Field(description="list of payload datasets"),
    ] = []
    type: TagTypeField = "runkey"
    name: Annotated[Optional[str], NameFieldDef]


class TagModel(TagBaseModel):
    objects: ObjectListField = []
    payloads: PayloadListField = []
    members: TagNameListField = []
    groups: TagNameListField = []


class ReuseModel(BaseModel):
    reuse_id: IDField


class ReuseConnectionModel(ReuseModel):
    view: ViewField = 1
    reuse_id: IDField


class ReuseObjectModel(ObjectBaseModel):
    children: ObjectReuseField = []
    payloads: PayloadReuseField = []


class FullTreeModel(TagBaseModel):
    name: Annotated[Optional[str], NameFieldDef]
    data: ReuseObjectModel
    type: TagTypeField = "runkey"
    payloads: PayloadListField = []


class CopyTreeModel(TagBaseModel):
    identifier: Annotated[str, Field(description="name of tag")]
    view: ViewField = 1
    type: TagTypeField = "runkey"


class InsertObjectsModel(BaseModel):
    list: Annotated[
        List[ObjectPayloadModel],
        Field(description="list of object datasets which can contain payload datasets"),
    ]


class InsertModel(BaseModel):
    list: Annotated[
        List[dict],
        Field(description="list of any datasets to put into the database"),
    ]


class AddToNodeModel(BaseModel):
    children: ChildrenConnField = []
    payloads: PayloadIDListField = []


class SearchModel(BaseModel):
    search_dict: SearchField


class OptionalSearchModel(BaseModel):
    search_dict: SearchField = None
    payload_types: Annotated[List[PayloadTypeField], Field(description="List of payload types to search for")] = []
    object_ids: Annotated[List[IDField], Field(description="List of payload types to search for")] = []


class PDBModuleImportModel(TagBaseModel):
    serials: Annotated[List[str], Field(description="list of serials to import")]
    type: TagTypeField = "runkey"
    itkdb_access_code1: PDBCodeField
    itkdb_access_code2: PDBCodeField


class UpdatePayloadAssociationModel(BaseModel):
    object_id: IDField
    payload_id: IDField


class UpdateObjectConnectionModel(BaseModel):
    parent: IDField
    child: IDField
    view: ViewField


class UpdateConnectionsModel(BaseModel):
    connections: Annotated[
        List[UpdateObjectConnectionModel],
        Field(description="list of parent-child connections and their new view value"),
    ]


class UpdatePayloadModel(BaseModel):
    associations: Annotated[
        List[UpdatePayloadAssociationModel],
        Field(description="list of object-payload associations for which to update paylaod"),
    ]
    changes: Annotated[
        dict[str, Any],
        Field(description="dict with data to update in payload"),
    ]
