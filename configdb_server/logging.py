import logging


def configure_logger(filename):
    addLoggingLevelBenchmark()

    log = logging.getLogger()

    while log.hasHandlers():
        log.removeHandler(log.handlers[0])

    log.setLevel(logging.BENCHMARK)
    log.addHandler(DebugFileHandler(filename=filename, mode="a"))
    log.addHandler(DebugStreamHandler())


def addLoggingLevelBenchmark():
    addLoggingLevel("BENCHMARK", logging.DEBUG - 5)


class DebugFileHandler(logging.FileHandler):
    def emit(self, record):
        if record.levelno == logging.BENCHMARK:
            super().emit(record)


class DebugStreamHandler(logging.StreamHandler):
    def emit(self, record):
        if record.levelno != logging.BENCHMARK:
            super().emit(record)


def addLoggingLevel(levelName, levelNum, methodName=None):
    if not methodName:
        methodName = levelName.lower()

    if hasattr(logging, levelName):
        return 0
        # raise AttributeError("{} already defined in logging module".format(levelName))
    if hasattr(logging, methodName):
        return 0
        # raise AttributeError("{} already defined in logging module".format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
        return 0
        # raise AttributeError("{} already defined in logger class".format(methodName))

    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, args, **kwargs)

    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot)
