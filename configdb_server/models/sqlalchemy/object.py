from configdb_server.models.sqlalchemy.models import Base, object_payload, tag_object, object_metadata, ObjectClosure
from configdb_server.models.model import ObjectModel
from sqlalchemy import Column, String, exists, event
from sqlalchemy.orm import relationship, Session
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy_utils import UUIDType
from configdb_server.models.sqlalchemy.payload import Payload
from configdb_server.models.sqlalchemy.metadata import Metadata
from configdb_server.exceptions import DatasetNotFoundError, LoopError
from uuid import uuid4, UUID
import os


@event.listens_for(Session, "after_flush")
def delete_orphaned_payloads(session, context):
    for object in session.deleted:
        if isinstance(object, Object):
            for payload in object.payloads:
                if not payload.objects and not payload.tags:
                    session.delete(payload)
            for meta in object.meta:
                if not meta.objects and not meta.tags:
                    session.delete(meta)


class Object(Base, ObjectModel):
    """
    ORM-class that represents the object-table
    """

    __tablename__ = "object"

    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    type = Column(String(255), nullable=False)

    tags = relationship("Tag", secondary=tag_object, back_populates="objects")
    payloads = relationship("Payload", secondary=object_payload, back_populates="objects")
    meta = relationship("Metadata", secondary=object_metadata, back_populates="objects")

    descendant_associations = relationship(
        "ObjectClosure", primaryjoin="ObjectClosure.ancestor_id == Object.id", back_populates="ancestor", cascade="all, delete"
    )
    ancestor_associations = relationship(
        "ObjectClosure", primaryjoin="ObjectClosure.descendant_id == Object.id", back_populates="descendant", cascade="all, delete"
    )

    descendants = association_proxy("descendant_associations", "descendant")
    ancestors = association_proxy("ancestor_associations", "ancestor")

    def __init__(self, session, type, children=[], parents=[], payloads=[], tags=[], id=None):
        if id is not None:
            self.id = UUID(id)

        self.type = type
        # Add self to closure table
        session.add(ObjectClosure(ancestor=self, descendant=self, depth=0))

        self.add_children(session, children)
        self.add_parents(session, parents)
        self.connect_children_parents(session)
        self.add_payloads(session, payloads)
        self.add_tags(session, tags)

    # def update(self, session, children, payloads, delete):
    #     if delete:
    #         self.remove_payloads(self.payloads)
    #         session.flush()
    #     if payloads:
    #         self.add_payloads(session, payloads)

    #     if delete:
    #         self.remove_children(self.descendants)
    #         session.flush()
    #     if children:
    #         self.add_children(session, children)

    def add_children(self, session, children):
        session.flush()
        for dataset in children:
            with session.no_autoflush:
                asso = session.query(Object).filter_by(id=dataset.id).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                for closure in asso.descendant_associations:
                    self.add_if_not_exist(session, ancestor=self, descendant=closure.descendant, depth=closure.depth + 1, view=dataset.view)
                    self.connect_children_parents(session)
        return True

    def add_parents(self, session, parents):
        session.flush()
        for dataset in parents:
            with session.no_autoflush:
                asso = session.query(Object).filter_by(id=dataset.id).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                # Add parent and ancestors of parents to closure table
                for closure in asso.ancestor_associations:
                    self.add_if_not_exist(session, ancestor=closure.ancestor, descendant=self, depth=closure.depth + 1, view=dataset.view)
                    self.connect_children_parents(session)
        return True

    def add_tags(self, session, tags):
        session.flush()
        for tag in tags:
            with session.no_autoflush:
                from configdb_server.models.sqlalchemy.tag import Tag

                asso = session.query(Tag).filter_by(name=tag).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                self.tags.append(asso)
        return True

    def connect_children_parents(self, session):
        session.flush()

        for desc in self.descendant_associations:
            if desc.depth != 0:
                for anc in self.ancestor_associations:
                    if anc.depth != 0:
                        self.add_if_not_exist(session, ancestor=anc.ancestor, descendant=desc.descendant, depth=anc.depth + desc.depth)  # , view=self.view

    def remove_children(self, session, children):
        for dataset in children:
            with session.no_autoflush:
                asso = session.query(ObjectClosure).filter_by(descendant_id=dataset).filter_by(ancestor_id=self.id.hex).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                asso.view = 2  # |= 2

    def add_payloads(self, session, payloads):
        for dataset in payloads:
            with session.no_autoflush:
                asso = session.query(Payload).filter_by(id=dataset).first()
            if asso is None:
                meta = self.add_metadata(session, dataset)
                if not meta:
                    session.rollback()
                    raise DatasetNotFoundError
            else:
                self.payloads.append(asso)
        return True

    def remove_payloads(self, session, payloads):
        for dataset in payloads:
            with session.no_autoflush:
                asso = session.query(Payload).filter_by(id=dataset).first()
            if asso is None:
                meta = self.remove_metadata(session, dataset)
                if not meta:
                    session.rollback()
                    raise DatasetNotFoundError
            else:
                self.payloads.remove(asso)
        return True

    def add_metadata(self, session, dataset):
        with session.no_autoflush:
            asso = session.query(Metadata).filter_by(id=dataset).first()
        if asso is None:
            session.rollback()
            raise DatasetNotFoundError
        else:
            self.meta.append(asso)
        return True

    def remove_metadata(self, session, dataset):
        with session.no_autoflush:
            asso = session.query(Metadata).filter_by(id=dataset).first()
        if asso is None:
            session.rollback()
            raise DatasetNotFoundError
        else:
            self.meta.remove(asso)
        return True

    def to_dict(self, view=1, connections=True, payload_data=True):
        rep = {"id": self.id.hex, "type": self.type}

        rep["metadata"] = {}
        for dataset in self.meta:
            rep["metadata"].update(dataset.get_data(dump=False))

        if connections:
            rep["payloads"] = []
            rep["metadata_ids"] = []
            rep["children"] = []

            for dataset in self.meta:
                rep["metadata_ids"].append(dataset.id.hex)

            for association in self.descendant_associations:
                if association.depth == 1:
                    if association.view & view > 0:
                        rep["children"].append(association.descendant.id.hex)

            for payload in self.payloads:
                rep["payloads"].append(payload.id.hex)

        return rep

    def to_tree(self, payload_data=False, payload_filter="", decode=True, format=False, depth=-1, view=1):
        rep = {"id": self.id.hex, "type": self.type}

        rep["metadata"] = {}
        rep["payloads"] = []
        rep["children"] = []

        for dataset in self.meta:
            rep["metadata"].update(dataset.get_data(dump=False))
            rep["payloads"].append(dataset.to_dict(format=format, payload_data=payload_data))

        for payload in self.payloads:
            if not payload_filter or payload_filter in payload.type:
                rep["payloads"].append(payload.to_dict(payload_data=payload_data, decode=decode, format=format))

        if depth != 0:
            depth = depth - 1
            for association in self.descendant_associations:
                if association.depth == 1:
                    if association.view & view > 0:
                        # if not view & 2 and association.view & 2:
                        #     continue
                        child = association.descendant
                        child_dict = child.to_tree(payload_data=payload_data, payload_filter=payload_filter, decode=decode, depth=depth, view=view)
                        child_dict["view"] = association.view
                        rep["children"].append(child_dict)
            depth = depth + 1
        return rep

    def add_if_not_exist(self, session, ancestor, descendant, depth, view=1):
        session.flush()
        # Check in database
        if ancestor == descendant and depth != 0:
            session.rollback()
            raise LoopError
        connection = session.query(ObjectClosure).filter_by(ancestor_id=ancestor.id, descendant_id=descendant.id, depth=depth).first()

        if not connection:
            connection = ObjectClosure(ancestor=ancestor, descendant=descendant, depth=depth, view=view)
            session.add(connection)
        else:
            connection.view = view
