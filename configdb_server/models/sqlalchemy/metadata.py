from configdb_server.models.sqlalchemy.models import Base, object_metadata, tag_metadata
from configdb_server.models.sqlalchemy.base_payload import BasePayload
from configdb_server.exceptions import NotValidJSonError
from sqlalchemy import Column, JSON
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
import json


class Metadata(BasePayload, Base):
    """
    ORM-class that represents the metadata-table
    """

    __tablename__ = "metadata"

    data = Column(JSONB())

    tags = relationship("Tag", secondary=tag_metadata, back_populates="meta")
    objects = relationship("Object", secondary=object_metadata, back_populates="meta")

    def __init__(self, session, type, data=None, name=None, id=None, tags=[], objects=[]):
        super().__init__(session, type, data, name, id, tags, objects)

    def to_dict(self, payload_data=True, format=False, connections=True):
        rep = {"id": self.id.hex, "type": self.type, "name": self.name, "meta": True}
        if payload_data:
            rep["data"] = self.get_data(format=format)
        return rep

    def get_data(self, dump=True, format=False):
        if dump:
            if format:
                return json.dumps(self.data, indent=4)
            else:
                return json.dumps(self.data)
        else:
            return self.data

    def set_data(self, data, encode=False):
        if isinstance(data, str):
            try:
                data = json.loads(data)
            except json.JSONDecodeError:
                raise NotValidJSonError
        self.data = data
