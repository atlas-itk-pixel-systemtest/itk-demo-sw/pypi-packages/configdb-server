from configdb_server.models.sqlalchemy.models import Base, tag_object, tag_payload, tag_metadata, tag_tag
from configdb_server.models.sqlalchemy.object import Object
from configdb_server.models.sqlalchemy.payload import Payload
from configdb_server.models.sqlalchemy.metadata import Metadata
from configdb_server.models.model import TagModel
from configdb_server.exceptions import DatasetNotFoundError, NameNotValidError
from configdb_server.tools import check_if_uuid
from sqlalchemy import Column, String, DateTime, event
from sqlalchemy.orm import relationship, Session
from sqlalchemy_utils import UUIDType

from uuid import uuid4
import os
import datetime


@event.listens_for(Session, "after_flush")
def delete_orphaned_payloads(session, context):
    for tag in session.deleted:
        if isinstance(tag, Tag):
            for payload in tag.payloads:
                if not payload.objects and not payload.tags:
                    session.delete(payload)
            for meta in tag.meta:
                if not meta.objects and not meta.tags:
                    session.delete(meta)


class Tag(TagModel, Base):
    """
    ORM-class that represents the tag-table
    """

    __tablename__ = "tag"

    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    name = Column(String(255), index=True, nullable=False, unique=True)
    author = Column(String(255), index=True)
    type = Column(String(255), index=True)
    time = Column(DateTime(), index=False, default=datetime.datetime.now(datetime.timezone.utc))
    comment = Column(String(255), index=False)

    members = relationship("Tag", secondary=tag_tag, primaryjoin=id == tag_tag.c.group_id, secondaryjoin=id == tag_tag.c.member_id, back_populates="groups")
    groups = relationship("Tag", secondary=tag_tag, primaryjoin=id == tag_tag.c.member_id, secondaryjoin=id == tag_tag.c.group_id, back_populates="members")

    payloads = relationship("Payload", secondary=tag_payload, back_populates="tags", cascade="all, delete")
    objects = relationship("Object", secondary=tag_object, back_populates="tags", cascade="all, delete")
    meta = relationship("Metadata", secondary=tag_metadata, back_populates="tags", cascade="all, delete")

    def __init__(self, session, name, type, objects=[], payloads=[], members=[], groups=[], author=None, id=None, comment=None, time=None):
        if id is not None:
            self.id = id

        if name == "" or check_if_uuid(name):
            raise NameNotValidError

        self.name = name  # .lower()
        self.author = author
        self.type = type
        self.comment = comment

        if time:
            self.time = time

        self.add_objects(session, objects)
        self.add_payloads(session, payloads)
        self.add_members(session, members)
        self.add_groups(session, groups)

    def update(self, session, objects=[], payloads=[], members=[], groups=[], delete=False, time=None):
        if time:
            self.time = time

        if delete:
            self.remove_objects(session, self.objects)
            for meta in self.meta:
                self.remove_metadata(session, meta)
            self.remove_payloads(session, self.payloads)
            self.remove_members(session, self.members)
            self.remove_groups(session, self.groups)
            session.flush()
        if payloads:
            self.add_payloads(session, payloads)
        if objects:
            self.add_objects(session, objects)
        if members:
            self.add_members(session, members)
        if groups:
            self.add_groups(session, groups)

    def add_objects(self, session, children):
        for dataset in children:
            with session.no_autoflush:
                asso = session.query(Object).filter_by(id=dataset).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                self.objects.append(asso)
        return True

    def remove_objects(self, session, children):
        for dataset in children:
            if isinstance(dataset, str):
                with session.no_autoflush:
                    asso = session.query(Object).filter_by(id=dataset).first()
                if asso is None:
                    session.rollback()
                    raise DatasetNotFoundError
            else:
                asso = dataset

            self.objects.remove(asso)
        return True

    def add_members(self, session, members):
        for dataset in members:
            with session.no_autoflush:
                asso = session.query(Tag).filter_by(name=dataset).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                self.members.append(asso)
        return True

    def remove_members(self, session, members):
        for dataset in members:
            if isinstance(dataset, str):
                with session.no_autoflush:
                    asso = session.query(Tag).filter_by(name=dataset).first()
                if asso is None:
                    session.rollback()
                    raise DatasetNotFoundError
            else:
                asso = dataset

            self.members.remove(asso)
        return True

    def add_groups(self, session, groups):
        for dataset in groups:
            with session.no_autoflush:
                asso = session.query(Tag).filter_by(name=dataset).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                self.groups.append(asso)
        return True

    def remove_groups(self, session, groups):
        for dataset in groups:
            if isinstance(dataset, str):
                with session.no_autoflush:
                    asso = session.query(Tag).filter_by(name=dataset).first()
                if asso is None:
                    session.rollback()
                    raise DatasetNotFoundError
            else:
                asso = dataset

            self.groups.remove(asso)
        return True

    def add_payloads(self, session, payloads):
        for dataset in payloads:
            with session.no_autoflush:
                asso = session.query(Payload).filter_by(id=dataset).first()
            if asso is None:
                meta = self.add_metadata(session, dataset)
                if not meta:
                    session.rollback()
                    raise DatasetNotFoundError
            else:
                self.payloads.append(asso)
        return True

    def remove_payloads(self, session, payloads):
        for dataset in payloads:
            if isinstance(dataset, str):
                with session.no_autoflush:
                    asso = session.query(Payload).filter_by(id=dataset).first()
                if asso is None:
                    meta = self.remove_metadata(session, dataset)
                    if not meta:
                        session.rollback()
                        raise DatasetNotFoundError
            else:
                asso = dataset
            self.payloads.remove(asso)
        return True

    def add_metadata(self, session, dataset):
        with session.no_autoflush:
            asso = session.query(Metadata).filter_by(id=dataset).first()
        if asso is None:
            session.rollback()
            raise DatasetNotFoundError
        else:
            self.meta.append(asso)
        return True

    def remove_metadata(self, session, dataset):
        if isinstance(dataset, str):
            with session.no_autoflush:
                asso = session.query(Metadata).filter_by(id=dataset).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
        else:
            asso = dataset

        self.meta.remove(asso)
        return True

    def to_dict(self, connections=True, payload_data=True):
        rep = {
            "time": self.time.isoformat(),
            "name": self.name,
            "id": self.id.hex,
            "author": self.author,
            "type": self.type,
            "comment": self.comment,
            "metadata": {},
        }

        for dataset in self.meta:
            rep["metadata"].update(dataset.get_data(dump=False))

        if connections:
            rep["groups"] = []
            rep["members"] = []
            rep["payloads"] = []
            rep["objects"] = []
            rep["metadata_ids"] = []
            for payload in self.payloads:
                rep["payloads"].append(payload.id.hex)
            for dataset in self.meta:
                rep["metadata_ids"].append(dataset.id.hex)
            for object in self.objects:
                rep["objects"].append(object.id.hex)
            for groups in self.groups:
                rep["groups"].append(groups.name)
            for members in self.members:
                rep["members"].append(members.name)
        return rep

    def to_tree(self, payload_data=0, payload_filter="", decode=True, format=False, depth=-1, view=1):
        rep = {
            "time": self.time.isoformat(),
            "name": self.name,
            "id": self.id.hex,
            "author": self.author,
            "comment": self.comment,
            "type": self.type,
            "payloads": [],
            "objects": [],
            "groups": [],
            "members": [],
            "metadata": {},
        }
        for payload in self.payloads:
            if not payload_filter or payload_filter in payload.type:
                rep["payloads"].append(payload.to_dict(payload_data=payload_data, decode=decode, format=format))
        for tag in self.groups:
            rep["groups"].append(tag.name)
        for tag in self.members:
            rep["members"].append(tag.to_tree(payload_data=payload_data, payload_filter=payload_filter, decode=decode, depth=depth, view=view))
        for dataset in self.meta:
            rep["metadata"].update(dataset.get_data(dump=False))
            rep["payloads"].append(dataset.to_dict(format=format, payload_data=payload_data))
        if depth != 0:
            for object in self.objects:
                rep["objects"].append(object.to_tree(payload_data=payload_data, payload_filter=payload_filter, decode=decode, depth=depth, view=view))
        return rep
