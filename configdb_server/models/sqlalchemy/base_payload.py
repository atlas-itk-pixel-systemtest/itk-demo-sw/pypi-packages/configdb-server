from configdb_server.models.model import PayloadModel
from configdb_server.exceptions import DatasetNotFoundError
from sqlalchemy import Column, String
from sqlalchemy_utils import UUIDType
from uuid import uuid4
from abc import ABC, abstractmethod


class BasePayload(PayloadModel, ABC):
    """
    ORM-class that represents the payload-table
    """

    __tablename__ = "payload"

    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    type = Column(String(255), nullable=False)
    name = Column(String(255))

    def __init__(self, session, type, data=None, name=None, id=None, tags=[], objects=[], encode=True):
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if data is not None:
            self.set_data(data, encode)
        self.type = type
        self.add_objects(session, objects)
        self.add_tags(session, tags)

    def to_tree(self, payload_data=0, depth=-1):
        return self.to_dict(payload_data=payload_data)

    @abstractmethod
    def get_data(self):
        pass

    @abstractmethod
    def set_data(self):
        pass

    def add_tags(self, session, tags):
        session.flush()
        for tag in tags:
            with session.no_autoflush:
                from configdb_server.models.sqlalchemy.tag import Tag

                asso = session.query(Tag).filter_by(name=tag).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                self.tags.append(asso)
        return True

    def add_objects(self, session, objects):
        session.flush()
        for obj in objects:
            with session.no_autoflush:
                from configdb_server.models.sqlalchemy.object import Object

                asso = session.query(Object).filter_by(id=obj).first()
            if asso is None:
                session.rollback()
                raise DatasetNotFoundError
            else:
                self.objects.append(asso)
        return True
