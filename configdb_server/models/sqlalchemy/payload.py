from configdb_server.models.sqlalchemy.models import Base, object_payload, tag_payload
from configdb_server.tools import compressToBytes, decompressBytes
from configdb_server.models.sqlalchemy.base_payload import BasePayload
from sqlalchemy import Column, LargeBinary
from sqlalchemy.orm import relationship
import json


class Payload(BasePayload, Base):
    """
    ORM-class that represents the payload-table
    """

    __tablename__ = "payload"

    data = Column(LargeBinary(length=(2**32) - 1))

    tags = relationship("Tag", secondary=tag_payload, back_populates="payloads")
    objects = relationship("Object", secondary=object_payload, back_populates="payloads")

    def __init__(self, session, type, data=None, name=None, id=None, tags=[], objects=[], encode=True):
        super().__init__(session, type, data, name, id, tags, objects, encode)

    def to_dict(self, payload_data=True, decode=True, format=False, connections=True):
        rep = {"id": self.id.hex, "type": self.type, "name": self.name, "meta": False}
        if payload_data:
            if self.data is not None:
                rep["data"] = self.get_data(decode=decode, format=format)
        return rep

    def get_data(self, decode=True, format=False):
        if decode:
            if format:
                data = decompressBytes(self.data)
                try:
                    data = json.loads(data)
                    data = json.dumps(data, indent=4)
                except json.JSONDecodeError:
                    pass
                return data
            else:
                return decompressBytes(self.data)
        else:
            return self.data

    def set_data(self, data, encode=True):
        if encode:
            self.data = compressToBytes(data)
        else:
            self.data = data
