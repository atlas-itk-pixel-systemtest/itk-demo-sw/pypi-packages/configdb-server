from better_abc import abstract_attribute
from abc import ABC, abstractmethod


class BaseModel(ABC):
    @abstract_attribute
    def id(self):
        pass

    @abstract_attribute
    def type(self):
        pass

    @abstractmethod
    def __init__():
        pass

    @abstractmethod
    def to_dict(self) -> dict:
        pass

    @abstractmethod
    def to_tree(self) -> dict:
        pass

class TagModel(BaseModel, ABC):
    @abstract_attribute
    def name(self):
        pass

    @abstract_attribute
    def author(self):
        pass

    @abstract_attribute
    def id(self):
        pass

    @abstract_attribute
    def time(self):
        pass

    @abstract_attribute
    def payloads(self):
        pass

    @abstract_attribute
    def members(self):
        pass

    @abstract_attribute
    def groups(self):
        pass

    @abstract_attribute
    def objects(self):
        pass


class PayloadBase(BaseModel, ABC):
    @abstract_attribute
    def data(self):
        pass

    @abstract_attribute
    def name(self):
        pass

    @abstract_attribute
    def tags(self):
        pass

    @abstract_attribute
    def objects(self):
        pass


class PayloadModel(PayloadBase, ABC):
    @abstractmethod
    def to_dict(self, payload_data: bool) -> dict:
        pass


class MetadataModel(PayloadBase, ABC):
    pass


class ObjectModel(BaseModel, ABC):
    @abstract_attribute
    def tags(self):
        pass

    @abstract_attribute
    def payloads(self):
        pass

    @abstract_attribute
    def ancestors(self):
        pass

    @abstract_attribute
    def descendants(self):
        pass

    # For the staging area:

    # @abstractmethod
    # def update():
    #     pass

    @abstractmethod
    def add_children():
        pass

    # @abstractmethod
    # def remove_children():
    #     pass

    @abstractmethod
    def add_payloads():
        pass

    # @abstractmethod
    # def remove_payloads():
    #     pass

    @abstractmethod
    def to_dict(self) -> dict:
        pass


class ClosureModel(ABC):
    @abstract_attribute
    def ancestor_id(self):
        pass

    @abstract_attribute
    def descendant_id(self):
        pass

    @abstract_attribute
    def ancestor(self):
        pass

    @abstract_attribute
    def descendant(self):
        pass

    @abstract_attribute
    def depth(self):
        pass

    @abstractmethod
    def __init__():
        pass

    # @abstractmethod
    # def to_dict(self, payload_data: bool) -> dict:
    #     pass

