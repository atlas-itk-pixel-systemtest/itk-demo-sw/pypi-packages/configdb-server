from abc import ABC
import re


class CustomError(Exception, ABC):
    id = 0
    title = ""
    text = ""

    def __str__(self):
        return f"{self.title}: {self.text}"


class WrongTypeError(CustomError):
    def __init__(self, type="", table=""):
        super().__init__()
        self.id = 461
        self.title = "Wrong type"
        self.text = f"The type {type} does not exist for the table {table}, please check the type property and try again."


class WrongTableError(CustomError):
    id = 462
    title = "Table not found"
    text = "The requested table does not exist, please check the table property and try again."


class DatasetNotFoundError(CustomError):
    id = 463
    title = "Dataset not found"
    text = "Task failed, one or more of the requested/given datasets do not exist."


class NameInUseError(CustomError):
    def __init__(self, name=""):
        super().__init__()
        self.id = 464
        self.title = "Name already in use"
        self.text = f"The given name {name} is already in use, please retry with a different name."


class IDInUseError(CustomError):
    def __init__(self, uuid="", reg=""):
        super().__init__()
        self.id = 465
        self.title = "UUID already in use"

        if reg:
            pattern = r"[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|[0-9a-fA-F]{32}"
            match = re.search(pattern, reg)
            if match:
                uuid = match.group(0)

        self.text = f"The given uuid: {uuid} is already in use, please retry with a different id."


class PayloadTypeError(CustomError):
    def __init__(self, type="", expected_type="", payl=""):
        super().__init__()
        self.id = 466
        self.title = "Wrong payload type"
        self.text = f"The type {type} of the payload {payl} does not match the expected type {expected_type}."


class EmptyNameError(CustomError):
    id = 467
    title = "Name can not be empty"
    text = "The name of a tag can not be empty."


class AttributeNotFoundError(CustomError):
    id = 468
    title = "Attribute not found"
    text = "The attribute which the list should be ordered by does not exist."


class DuplicationError(CustomError):
    id = 469
    title = "Dataset already in runkey"
    text = "A configuration/connectivity dataset can not be added multiple times into a runkey."


class EndpointNotAvailableError(CustomError):
    id = 470
    title = "Endpoint not available"
    text = "The endpoint is not available in current microservice state."


class NameNotValidError(CustomError):
    id = 471
    title = "Name is not valid."
    text = "Given name is not valid. Try again with another name (no UUID or empty string)."


class DatabaseOutdatedError(CustomError):
    id = 472
    title = "Database tables not up to date"
    text = (
        "Database tables are not up to date with the model description. Upgrade/Downgrade the database with the corresponding endpoints."
    )


class DatabaseNotExistingError(CustomError):
    id = 473
    title = "Database configdb does not exist"
    text = "Database confgidb does not exist on database servers. Create it with database/create_db endpoint."


class DatabaseNotConnectedError(CustomError):
    id = 474
    title = "Can't connect to backend database"
    text = "Can't connect to backend database. Make sure the database servers are running."


class FeatureNotSupported(CustomError):
    id = 475
    title = "Feature is not supported"
    text = "Feature is not supported for this backend"


class WrongParameterError(CustomError):
    id = 476
    title = "Parameter is not valid"
    text = "Parameter for order or filter by does not exist."


class NotValidJSonError(CustomError):
    def __init__(self, payl=""):
        super().__init__()
        self.id = 477
        self.title = "Payload is not valid JSON"
        self.text = f"Payload {payl} is not valid JSON. Please check the payload and try again."


class DictTypeError(CustomError):
    id = 478
    title = "Dictionary missing type values"
    text = "The given dictionary misses required type values. Please make sure all dataset contain a type."


class DictIDError(CustomError):
    id = 479
    title = "Dictionary missing UUID values"
    text = "The given dictionary misses required UUID values or the sent ID are not valid UUIDs. Please make sure all ids are valid."


class PacketSizeError(CustomError):
    id = 479
    title = "Got a packet bigger than 'max_allowed_packet'"
    text = "The insert operation failed because its packet size was higher than the maximum supported package size by the backend database. Please split it into multiple inserts."


class NotValidUUIDError(CustomError):
    id = 480
    title = "Given ID is not a valid UUID"
    text = "The ID that was given is not in the correct format. Please make sure that your ID is a valid UUID4."


class EmptyTagError(CustomError):
    id = 481
    title = "Tag does not point to a runkey"
    text = "The given tag does not point to a runkey."


class LoopError(CustomError):
    id = 482
    title = "Database operation would create a loop."
    text = "Database operation would create a loop, which is currently not supported."


class PayloadFormatError(CustomError):
    def __init__(self, payl1="", payl2="", missing_key=""):
        super().__init__()
        self.id = 483
        self.title = "Mismatch in payload format"
        self.text = f"Keys {missing_key} from payload {payl1} are missing in payload {payl2}."


class ChangeFormatError(CustomError):
    def __init__(self, type1="", type2="", change="", text=""):
        super().__init__()
        self.id = 484
        self.title = "Mismatch in change format"
        if not text:
            self.text = f"Change {change} expects {type1} but payload contains {type2}."
        else:
            self.text = text


"""
"400": {
    "title": "Wrong data",
    "text": "The endpoint expects different data. Please refer to the documentation for additional information."
},
"404": {
    "title": "Endpoint not found",
    "text": "Endpoint not found, please refer to the documentation for an overview of available endpoints"
},
"411": {
    "title": "Endpoint not available for table",
    "text": "The endpoint is not available for the specified table or the table does not exist."
},
"413": {
    "title": "Transition not possible",
    "text": "Transition to next state is not possible, state was locked. Try again."
},
"500": {
    "title": "Internal server error",
    "text": "Internal server error, please contact the admin and report this problem."
},
"501": {
    "title": "Backend database not available",
    "text": "Backend database of the configdb not available. Check if database server is still running or sqlite file has been deleted."
}
"""
